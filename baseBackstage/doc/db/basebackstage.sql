/*
Navicat MySQL Data Transfer

Source Server         : 本地环境
Source Server Version : 50703
Source Host           : localhost:3306
Source Database       : basebackstage

Target Server Type    : MYSQL
Target Server Version : 50703
File Encoding         : 65001

Date: 2016-11-23 17:46:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bf_resource
-- ----------------------------
DROP TABLE IF EXISTS `bf_resource`;
CREATE TABLE `bf_resource` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '资源名称',
  `url` varchar(100) DEFAULT NULL COMMENT '访问URL',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `pid` int(19) DEFAULT '0' COMMENT '父级ID',
  `seq` int(2) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态（0启用，1禁用）',
  `resourcetype` int(2) NOT NULL DEFAULT '0' COMMENT '资源类型（0菜单，1功能）',
  `createdate` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1131 DEFAULT CHARSET=utf8 COMMENT='资源';

-- ----------------------------
-- Records of bf_resource
-- ----------------------------
INSERT INTO `bf_resource` VALUES ('1', '系统管理', '/', '系统管理', 'icon-company', '0', '0', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `bf_resource` VALUES ('2', '用户管理', '/user/userPage', '用户管理', 'icon-company', '1', '0', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `bf_resource` VALUES ('3', '角色管理', '/role/rolePage', '角色管理', 'icon-company', '1', '0', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `bf_resource` VALUES ('4', '资源管理', '/resource/resourcePage', '资源管理', 'icon-company', '1', '0', '0', '0', '2014-02-19 01:00:00');
INSERT INTO `bf_resource` VALUES ('1033', '用户管理-查看', '/user/userData', '用户管理-查看', '', '2', '1', '0', '1', '2016-07-14 18:50:48');
INSERT INTO `bf_resource` VALUES ('1034', '用户管理-编辑页面', '/user/userEditPage', '用户管理-编辑页面', '', '2', '1', '0', '1', '2016-07-14 18:52:03');
INSERT INTO `bf_resource` VALUES ('1035', '用户管理-添加', '/user/userAdd', '用户管理-添加', '', '2', '1', '0', '1', '2016-07-14 18:53:13');
INSERT INTO `bf_resource` VALUES ('1036', '用户管理-修改', '/user/userUpdate', '用户管理-修改', '', '2', '1', '0', '1', '2016-07-14 18:53:49');
INSERT INTO `bf_resource` VALUES ('1037', '角色管理-查看', '/role/roleData', '角色管理-查看', '', '3', '1', '0', '1', '2016-07-14 18:57:21');
INSERT INTO `bf_resource` VALUES ('1038', '角色管理-授权页面', '/role/roleAuthorityPage', '角色管理-授权页面', '', '3', '1', '0', '1', '2016-07-14 18:58:26');
INSERT INTO `bf_resource` VALUES ('1039', '资源管理-查看', '/resource/resourceData', '资源管理-查看', '', '4', '1', '0', '1', '2016-07-14 18:59:15');
INSERT INTO `bf_resource` VALUES ('1040', '资源管理-编辑页面', '/resource/resourceEditPage', '资源管理-编辑页面', '', '4', '1', '0', '1', '2016-07-14 18:59:54');
INSERT INTO `bf_resource` VALUES ('1041', '资源管理-添加', '/resource/resourceAdd', '资源管理-添加', '', '4', '1', '0', '1', '2016-07-14 19:00:35');
INSERT INTO `bf_resource` VALUES ('1042', '资源管理-修改', '/resource/resourceUpdate', '资源管理-修改', '', '4', '1', '0', '1', '2016-07-14 19:00:54');
INSERT INTO `bf_resource` VALUES ('1043', '资源管理-左边菜单', '/resource/resourceMenuData', '资源管理-左边菜单', '', '4', '1', '0', '1', '2016-07-15 09:20:52');
INSERT INTO `bf_resource` VALUES ('1044', '资源管理-删除', '/resource/resourceDelete', '资源管理-删除', '', '4', '1', '0', '1', '2016-07-18 11:23:35');
INSERT INTO `bf_resource` VALUES ('1045', '角色管理-授权数据', '/role/resourceData', '角色管理-授权数据', '', '3', '1', '0', '1', '2016-07-18 11:26:19');
INSERT INTO `bf_resource` VALUES ('1046', '角色管理-授权操作', '/role/roleAuthorityOperation', '角色管理-授权操作', '', '3', '1', '0', '1', '2016-07-18 11:27:01');
INSERT INTO `bf_resource` VALUES ('1065', '用户管理-删除', '/user/userDelete', '用户管理-删除', '', '2', '1', '0', '1', '2016-07-19 13:01:21');
INSERT INTO `bf_resource` VALUES ('1066', '角色管理-编辑页面', '/role/roleEditPage', '角色管理-编辑页面', '', '3', '1', '0', '1', '2016-07-19 18:29:10');
INSERT INTO `bf_resource` VALUES ('1067', '角色管理-添加', '/role/roleAdd', '角色管理-添加', '1', '3', '1', '0', '1', '2016-07-19 18:29:52');
INSERT INTO `bf_resource` VALUES ('1068', '角色管理-修改', '/role/roleUpdate', '角色管理-修改', '', '3', '1', '0', '1', '2016-07-19 18:30:18');
INSERT INTO `bf_resource` VALUES ('1069', '角色管理-删除', '/role/roleDelete', '角色管理-删除', '1', '3', '1', '0', '1', '2016-07-19 18:30:52');
INSERT INTO `bf_resource` VALUES ('1085', '首页', '/', '首页', '', '-1', '1000', '0', '0', '2016-07-29 10:26:22');
INSERT INTO `bf_resource` VALUES ('1086', '首页-退出系统', '/home/exit', '首页-退出系统', '', '1085', '0', '0', '1', '2016-07-29 10:35:21');
INSERT INTO `bf_resource` VALUES ('1087', '首页-修改密码页面', '/home/updatePasswordPage', '首页-修改密码页面', '', '1085', '0', '0', '1', '2016-07-29 10:40:34');
INSERT INTO `bf_resource` VALUES ('1088', '首页-修改密码-验证旧密码', '/home/checkAffirmPassword', '首页-修改密码-验证旧密码', '', '1085', '0', '0', '1', '2016-07-29 14:29:01');
INSERT INTO `bf_resource` VALUES ('1089', '首页-修改密码', '/home/updatePassword', '首页-修改密码', '', '1085', '0', '0', '1', '2016-07-29 16:46:17');
INSERT INTO `bf_resource` VALUES ('1126', '报表管理', '/', '报表管理', '', '0', '1', '0', '0', '2016-11-23 16:51:10');
INSERT INTO `bf_resource` VALUES ('1127', '日志管理', '/sysLog/sysLogPage', '日志管理', '', '1126', '0', '0', '0', '2016-11-23 16:52:20');
INSERT INTO `bf_resource` VALUES ('1128', '日志管理-查看', '/sysLog/sysLogData', '日志管理-查看', '', '1127', '0', '0', '1', '2016-11-23 16:59:59');
INSERT INTO `bf_resource` VALUES ('1129', '日志管理-导出', '/sysLog/sysLogExcelExport', '日志管理-导出', '', '1127', '0', '0', '1', '2016-11-23 17:18:55');
INSERT INTO `bf_resource` VALUES ('1130', '日志管理-导出CSV', '/sysLog/sysLogCSVExport', '日志管理-导出CSV', '', '1127', '0', '0', '1', '2016-11-23 17:38:33');

-- ----------------------------
-- Table structure for bf_role
-- ----------------------------
DROP TABLE IF EXISTS `bf_role`;
CREATE TABLE `bf_role` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态（0启用，1禁用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of bf_role
-- ----------------------------
INSERT INTO `bf_role` VALUES ('1', '超级管理员', '超级管理员', '0');

-- ----------------------------
-- Table structure for bf_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `bf_role_resource`;
CREATE TABLE `bf_role_resource` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `role_id` int(19) NOT NULL COMMENT '角色ID',
  `resource_id` int(19) NOT NULL COMMENT '资源ID ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1982 DEFAULT CHARSET=utf8 COMMENT='角色资源';

-- ----------------------------
-- Records of bf_role_resource
-- ----------------------------
INSERT INTO `bf_role_resource` VALUES ('1949', '1', '1');
INSERT INTO `bf_role_resource` VALUES ('1950', '1', '2');
INSERT INTO `bf_role_resource` VALUES ('1951', '1', '1033');
INSERT INTO `bf_role_resource` VALUES ('1952', '1', '1034');
INSERT INTO `bf_role_resource` VALUES ('1953', '1', '1035');
INSERT INTO `bf_role_resource` VALUES ('1954', '1', '1036');
INSERT INTO `bf_role_resource` VALUES ('1955', '1', '1065');
INSERT INTO `bf_role_resource` VALUES ('1956', '1', '3');
INSERT INTO `bf_role_resource` VALUES ('1957', '1', '1037');
INSERT INTO `bf_role_resource` VALUES ('1958', '1', '1038');
INSERT INTO `bf_role_resource` VALUES ('1959', '1', '1045');
INSERT INTO `bf_role_resource` VALUES ('1960', '1', '1046');
INSERT INTO `bf_role_resource` VALUES ('1961', '1', '1066');
INSERT INTO `bf_role_resource` VALUES ('1962', '1', '1067');
INSERT INTO `bf_role_resource` VALUES ('1963', '1', '1068');
INSERT INTO `bf_role_resource` VALUES ('1964', '1', '1069');
INSERT INTO `bf_role_resource` VALUES ('1965', '1', '4');
INSERT INTO `bf_role_resource` VALUES ('1966', '1', '1039');
INSERT INTO `bf_role_resource` VALUES ('1967', '1', '1040');
INSERT INTO `bf_role_resource` VALUES ('1968', '1', '1041');
INSERT INTO `bf_role_resource` VALUES ('1969', '1', '1042');
INSERT INTO `bf_role_resource` VALUES ('1970', '1', '1043');
INSERT INTO `bf_role_resource` VALUES ('1971', '1', '1044');
INSERT INTO `bf_role_resource` VALUES ('1972', '1', '1126');
INSERT INTO `bf_role_resource` VALUES ('1973', '1', '1127');
INSERT INTO `bf_role_resource` VALUES ('1974', '1', '1128');
INSERT INTO `bf_role_resource` VALUES ('1975', '1', '1129');
INSERT INTO `bf_role_resource` VALUES ('1976', '1', '1130');
INSERT INTO `bf_role_resource` VALUES ('1977', '1', '1085');
INSERT INTO `bf_role_resource` VALUES ('1978', '1', '1086');
INSERT INTO `bf_role_resource` VALUES ('1979', '1', '1087');
INSERT INTO `bf_role_resource` VALUES ('1980', '1', '1088');
INSERT INTO `bf_role_resource` VALUES ('1981', '1', '1089');

-- ----------------------------
-- Table structure for bf_syslog
-- ----------------------------
DROP TABLE IF EXISTS `bf_syslog`;
CREATE TABLE `bf_syslog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `opt_content` varchar(1024) DEFAULT NULL,
  `client_ip` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2531 DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of bf_syslog
-- ----------------------------
INSERT INTO `bf_syslog` VALUES ('2332', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:23:35');
INSERT INTO `bf_syslog` VALUES ('2333', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 16:23:37');
INSERT INTO `bf_syslog` VALUES ('2334', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 16:23:37');
INSERT INTO `bf_syslog` VALUES ('2335', 'admin', '用户管理-查看', '[类名]:com.xiaowu.web.controller.system.UserController,[方法]:userData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:23:38');
INSERT INTO `bf_syslog` VALUES ('2336', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:23:39');
INSERT INTO `bf_syslog` VALUES ('2337', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:23:40');
INSERT INTO `bf_syslog` VALUES ('2338', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:23:40');
INSERT INTO `bf_syslog` VALUES ('2339', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=2&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:24:15');
INSERT INTO `bf_syslog` VALUES ('2340', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:37:43');
INSERT INTO `bf_syslog` VALUES ('2341', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:37:44');
INSERT INTO `bf_syslog` VALUES ('2342', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:37:44');
INSERT INTO `bf_syslog` VALUES ('2343', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:37:45');
INSERT INTO `bf_syslog` VALUES ('2344', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 16:37:48');
INSERT INTO `bf_syslog` VALUES ('2345', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 16:37:48');
INSERT INTO `bf_syslog` VALUES ('2346', 'admin', '用户管理-查看', '[类名]:com.xiaowu.web.controller.system.UserController,[方法]:userData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:37:51');
INSERT INTO `bf_syslog` VALUES ('2347', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:updatePasswordPage,[参数]:', '127.0.0.1', '2016-11-23 16:37:52');
INSERT INTO `bf_syslog` VALUES ('2348', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:37:55');
INSERT INTO `bf_syslog` VALUES ('2349', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:37:56');
INSERT INTO `bf_syslog` VALUES ('2350', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:37:56');
INSERT INTO `bf_syslog` VALUES ('2351', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:51:31');
INSERT INTO `bf_syslog` VALUES ('2352', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 16:51:33');
INSERT INTO `bf_syslog` VALUES ('2353', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 16:51:33');
INSERT INTO `bf_syslog` VALUES ('2354', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:51:36');
INSERT INTO `bf_syslog` VALUES ('2355', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:36');
INSERT INTO `bf_syslog` VALUES ('2356', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:51:36');
INSERT INTO `bf_syslog` VALUES ('2357', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:38');
INSERT INTO `bf_syslog` VALUES ('2358', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=4&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:41');
INSERT INTO `bf_syslog` VALUES ('2359', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:43');
INSERT INTO `bf_syslog` VALUES ('2360', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=2&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:50');
INSERT INTO `bf_syslog` VALUES ('2361', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:51:53');
INSERT INTO `bf_syslog` VALUES ('2362', 'admin', '资源管理-添加', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceAdd,[参数]:id=&name=日志管理-查看&resourcetype=0&seq=&icon=&pid=1126&status=0&url=/&description=日志管理-查看&', '127.0.0.1', '2016-11-23 16:52:20');
INSERT INTO `bf_syslog` VALUES ('2363', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:52:20');
INSERT INTO `bf_syslog` VALUES ('2364', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:52:20');
INSERT INTO `bf_syslog` VALUES ('2365', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=4&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:52:22');
INSERT INTO `bf_syslog` VALUES ('2366', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 16:58:26');
INSERT INTO `bf_syslog` VALUES ('2367', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 16:58:31');
INSERT INTO `bf_syslog` VALUES ('2368', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 16:58:31');
INSERT INTO `bf_syslog` VALUES ('2369', 'admin', '用户管理-查看', '[类名]:com.xiaowu.web.controller.system.UserController,[方法]:userData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:58:35');
INSERT INTO `bf_syslog` VALUES ('2370', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 16:58:36');
INSERT INTO `bf_syslog` VALUES ('2371', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:58:36');
INSERT INTO `bf_syslog` VALUES ('2372', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:58:36');
INSERT INTO `bf_syslog` VALUES ('2373', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:58:38');
INSERT INTO `bf_syslog` VALUES ('2374', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:58:40');
INSERT INTO `bf_syslog` VALUES ('2375', 'admin', '资源管理-修改', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceUpdate,[参数]:id=1127&name=日志管理-查看&resourcetype=0&seq=0&icon=&pid=1126&status=0&url=/sysLog/sysLogPage&description=日志管理-查看&', '127.0.0.1', '2016-11-23 16:58:51');
INSERT INTO `bf_syslog` VALUES ('2376', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:58:51');
INSERT INTO `bf_syslog` VALUES ('2377', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:58:51');
INSERT INTO `bf_syslog` VALUES ('2378', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=4&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:58:52');
INSERT INTO `bf_syslog` VALUES ('2379', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:00');
INSERT INTO `bf_syslog` VALUES ('2380', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:03');
INSERT INTO `bf_syslog` VALUES ('2381', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:14');
INSERT INTO `bf_syslog` VALUES ('2382', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=2&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:18');
INSERT INTO `bf_syslog` VALUES ('2383', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:18');
INSERT INTO `bf_syslog` VALUES ('2384', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:20');
INSERT INTO `bf_syslog` VALUES ('2385', 'admin', '资源管理-修改', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceUpdate,[参数]:id=1127&name=日志管理&resourcetype=0&seq=0&icon=&pid=1126&status=0&url=/sysLog/sysLogPage&description=日志管理&', '127.0.0.1', '2016-11-23 16:59:26');
INSERT INTO `bf_syslog` VALUES ('2386', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:59:26');
INSERT INTO `bf_syslog` VALUES ('2387', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:26');
INSERT INTO `bf_syslog` VALUES ('2388', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=2&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:27');
INSERT INTO `bf_syslog` VALUES ('2389', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:32');
INSERT INTO `bf_syslog` VALUES ('2390', 'admin', '资源管理-添加', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceAdd,[参数]:id=&name=日志管理-查看&resourcetype=1&seq=&icon=&pid=1127&status=0&url=/sysLog/sysLogData&description=日志管理-查看&', '127.0.0.1', '2016-11-23 16:59:59');
INSERT INTO `bf_syslog` VALUES ('2391', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 16:59:59');
INSERT INTO `bf_syslog` VALUES ('2392', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 16:59:59');
INSERT INTO `bf_syslog` VALUES ('2393', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:01');
INSERT INTO `bf_syslog` VALUES ('2394', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=4&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:02');
INSERT INTO `bf_syslog` VALUES ('2395', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:03');
INSERT INTO `bf_syslog` VALUES ('2396', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:05');
INSERT INTO `bf_syslog` VALUES ('2397', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:06');
INSERT INTO `bf_syslog` VALUES ('2398', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:09');
INSERT INTO `bf_syslog` VALUES ('2399', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:11');
INSERT INTO `bf_syslog` VALUES ('2400', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:12');
INSERT INTO `bf_syslog` VALUES ('2401', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:12');
INSERT INTO `bf_syslog` VALUES ('2402', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:13');
INSERT INTO `bf_syslog` VALUES ('2403', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:00:16');
INSERT INTO `bf_syslog` VALUES ('2404', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:00:16');
INSERT INTO `bf_syslog` VALUES ('2405', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:00:16');
INSERT INTO `bf_syslog` VALUES ('2406', 'admin', '角色管理-授权数据', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:resourceData,[参数]:id=1&', '127.0.0.1', '2016-11-23 17:00:18');
INSERT INTO `bf_syslog` VALUES ('2407', 'admin', '角色管理-授权操作', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleAuthorityOperation,[参数]:id=1&resources=1,2,1033,1034,1035,1036,1065,3,1037,1038,1045,1046,1066,1067,1068,1069,4,1039,1040,1041,1042,1043,1044,1126,1127,1128,1085,1086,1087,1088,1089,&', '127.0.0.1', '2016-11-23 17:00:23');
INSERT INTO `bf_syslog` VALUES ('2408', 'admin', '退出', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:logout,[参数]:', '127.0.0.1', '2016-11-23 17:00:25');
INSERT INTO `bf_syslog` VALUES ('2409', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:00:28');
INSERT INTO `bf_syslog` VALUES ('2410', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:00:30');
INSERT INTO `bf_syslog` VALUES ('2411', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:00:30');
INSERT INTO `bf_syslog` VALUES ('2412', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:00:33');
INSERT INTO `bf_syslog` VALUES ('2413', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:09:06');
INSERT INTO `bf_syslog` VALUES ('2414', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:09:10');
INSERT INTO `bf_syslog` VALUES ('2415', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:09:10');
INSERT INTO `bf_syslog` VALUES ('2416', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:09:13');
INSERT INTO `bf_syslog` VALUES ('2417', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:09:19');
INSERT INTO `bf_syslog` VALUES ('2418', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=admin&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:09:24');
INSERT INTO `bf_syslog` VALUES ('2419', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=admin&roleName=首页&clientIp=&', '127.0.0.1', '2016-11-23 17:09:28');
INSERT INTO `bf_syslog` VALUES ('2420', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=admin&roleName=首页&clientIp=.0.0.2&', '127.0.0.1', '2016-11-23 17:09:32');
INSERT INTO `bf_syslog` VALUES ('2421', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&status=&loginName=admin&roleName=首页&clientIp=.0.0.1&', '127.0.0.1', '2016-11-23 17:09:35');
INSERT INTO `bf_syslog` VALUES ('2422', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:09:42');
INSERT INTO `bf_syslog` VALUES ('2423', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:09:47');
INSERT INTO `bf_syslog` VALUES ('2424', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:10:23');
INSERT INTO `bf_syslog` VALUES ('2425', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:17:59');
INSERT INTO `bf_syslog` VALUES ('2426', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:18:02');
INSERT INTO `bf_syslog` VALUES ('2427', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:18:02');
INSERT INTO `bf_syslog` VALUES ('2428', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:08');
INSERT INTO `bf_syslog` VALUES ('2429', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=2&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:17');
INSERT INTO `bf_syslog` VALUES ('2430', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=3&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:17');
INSERT INTO `bf_syslog` VALUES ('2431', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=4&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:19');
INSERT INTO `bf_syslog` VALUES ('2432', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=5&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:20');
INSERT INTO `bf_syslog` VALUES ('2433', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=6&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:21');
INSERT INTO `bf_syslog` VALUES ('2434', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=7&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:18:21');
INSERT INTO `bf_syslog` VALUES ('2435', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:18:30');
INSERT INTO `bf_syslog` VALUES ('2436', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:30');
INSERT INTO `bf_syslog` VALUES ('2437', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:32');
INSERT INTO `bf_syslog` VALUES ('2438', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:37');
INSERT INTO `bf_syslog` VALUES ('2439', 'admin', '资源管理-添加', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceAdd,[参数]:id=&name=日志管理-导出&resourcetype=1&seq=&icon=&pid=&status=0&url=/sysLog/sysLogExcelExport&description=日志管理-导出&', '127.0.0.1', '2016-11-23 17:18:55');
INSERT INTO `bf_syslog` VALUES ('2440', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:18:55');
INSERT INTO `bf_syslog` VALUES ('2441', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:55');
INSERT INTO `bf_syslog` VALUES ('2442', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:56');
INSERT INTO `bf_syslog` VALUES ('2443', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:18:59');
INSERT INTO `bf_syslog` VALUES ('2444', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:18:59');
INSERT INTO `bf_syslog` VALUES ('2445', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1126&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:01');
INSERT INTO `bf_syslog` VALUES ('2446', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=4&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:01');
INSERT INTO `bf_syslog` VALUES ('2447', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=2&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:02');
INSERT INTO `bf_syslog` VALUES ('2448', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:04');
INSERT INTO `bf_syslog` VALUES ('2449', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=3&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:04');
INSERT INTO `bf_syslog` VALUES ('2450', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1085&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:05');
INSERT INTO `bf_syslog` VALUES ('2451', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:19:06');
INSERT INTO `bf_syslog` VALUES ('2452', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:07');
INSERT INTO `bf_syslog` VALUES ('2453', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:19:07');
INSERT INTO `bf_syslog` VALUES ('2454', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:19:21');
INSERT INTO `bf_syslog` VALUES ('2455', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:19:21');
INSERT INTO `bf_syslog` VALUES ('2456', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:19:21');
INSERT INTO `bf_syslog` VALUES ('2457', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:23');
INSERT INTO `bf_syslog` VALUES ('2458', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:19:23');
INSERT INTO `bf_syslog` VALUES ('2459', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:19:24');
INSERT INTO `bf_syslog` VALUES ('2460', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:19:32');
INSERT INTO `bf_syslog` VALUES ('2461', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:19:35');
INSERT INTO `bf_syslog` VALUES ('2462', 'admin', '角色管理-授权数据', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:resourceData,[参数]:id=1&', '127.0.0.1', '2016-11-23 17:19:36');
INSERT INTO `bf_syslog` VALUES ('2463', 'admin', '角色管理-授权操作', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleAuthorityOperation,[参数]:id=1&resources=1,2,1033,1034,1035,1036,1065,3,1037,1038,1045,1046,1066,1067,1068,1069,4,1039,1040,1041,1042,1043,1044,1126,1127,1128,1129,1085,1086,1087,1088,1089,&', '127.0.0.1', '2016-11-23 17:19:39');
INSERT INTO `bf_syslog` VALUES ('2464', 'admin', '退出', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:logout,[参数]:', '127.0.0.1', '2016-11-23 17:19:41');
INSERT INTO `bf_syslog` VALUES ('2465', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:19:44');
INSERT INTO `bf_syslog` VALUES ('2466', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:19:46');
INSERT INTO `bf_syslog` VALUES ('2467', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:19:46');
INSERT INTO `bf_syslog` VALUES ('2468', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:19:47');
INSERT INTO `bf_syslog` VALUES ('2469', 'admin', '日志管理-导出', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogExcelExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:19:50');
INSERT INTO `bf_syslog` VALUES ('2470', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:20:49');
INSERT INTO `bf_syslog` VALUES ('2471', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:20:54');
INSERT INTO `bf_syslog` VALUES ('2472', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:20:54');
INSERT INTO `bf_syslog` VALUES ('2473', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:20:56');
INSERT INTO `bf_syslog` VALUES ('2474', 'admin', '日志管理-导出', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogExcelExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:21:00');
INSERT INTO `bf_syslog` VALUES ('2475', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:26:05');
INSERT INTO `bf_syslog` VALUES ('2476', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:26:08');
INSERT INTO `bf_syslog` VALUES ('2477', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:26:08');
INSERT INTO `bf_syslog` VALUES ('2478', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:26:11');
INSERT INTO `bf_syslog` VALUES ('2479', 'admin', '日志管理-导出', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogExcelExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:26:14');
INSERT INTO `bf_syslog` VALUES ('2480', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:37:26');
INSERT INTO `bf_syslog` VALUES ('2481', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:37:30');
INSERT INTO `bf_syslog` VALUES ('2482', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:37:31');
INSERT INTO `bf_syslog` VALUES ('2483', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:37:37');
INSERT INTO `bf_syslog` VALUES ('2484', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:37:38');
INSERT INTO `bf_syslog` VALUES ('2485', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:37:40');
INSERT INTO `bf_syslog` VALUES ('2486', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:37:40');
INSERT INTO `bf_syslog` VALUES ('2487', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:37:41');
INSERT INTO `bf_syslog` VALUES ('2488', 'admin', '日志管理-导出', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogExcelExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:37:42');
INSERT INTO `bf_syslog` VALUES ('2489', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:37:51');
INSERT INTO `bf_syslog` VALUES ('2490', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:37:52');
INSERT INTO `bf_syslog` VALUES ('2491', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:37:52');
INSERT INTO `bf_syslog` VALUES ('2492', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:37:54');
INSERT INTO `bf_syslog` VALUES ('2493', 'admin', '资源管理-添加', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceAdd,[参数]:id=&name=日志管理-导出CSV&resourcetype=1&seq=&icon=&pid=1127&status=0&url=/sysLog/sysLogCSVExport&description=日志管理-导出CSV&', '127.0.0.1', '2016-11-23 17:38:33');
INSERT INTO `bf_syslog` VALUES ('2494', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:38:33');
INSERT INTO `bf_syslog` VALUES ('2495', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=1127&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:38:33');
INSERT INTO `bf_syslog` VALUES ('2496', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:38:34');
INSERT INTO `bf_syslog` VALUES ('2497', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:38:35');
INSERT INTO `bf_syslog` VALUES ('2498', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:38:35');
INSERT INTO `bf_syslog` VALUES ('2499', 'admin', '资源管理-左边菜单', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceMenuData,[参数]:resourcetype=0&', '127.0.0.1', '2016-11-23 17:38:37');
INSERT INTO `bf_syslog` VALUES ('2500', 'admin', '资源管理-查看', '[类名]:com.xiaowu.web.controller.system.ResourceController,[方法]:resourceData,[参数]:page=1&pid=0&startTime=&endTime=&', '127.0.0.1', '2016-11-23 17:38:37');
INSERT INTO `bf_syslog` VALUES ('2501', 'admin', '角色管理-查看', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleData,[参数]:page=1&status=&name=&description=&', '127.0.0.1', '2016-11-23 17:38:38');
INSERT INTO `bf_syslog` VALUES ('2502', 'admin', '角色管理-授权数据', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:resourceData,[参数]:id=1&', '127.0.0.1', '2016-11-23 17:38:39');
INSERT INTO `bf_syslog` VALUES ('2503', 'admin', '角色管理-授权操作', '[类名]:com.xiaowu.web.controller.system.RoleController,[方法]:roleAuthorityOperation,[参数]:id=1&resources=1,2,1033,1034,1035,1036,1065,3,1037,1038,1045,1046,1066,1067,1068,1069,4,1039,1040,1041,1042,1043,1044,1126,1127,1128,1129,1130,1085,1086,1087,1088,1089,&', '127.0.0.1', '2016-11-23 17:38:42');
INSERT INTO `bf_syslog` VALUES ('2504', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:38:44');
INSERT INTO `bf_syslog` VALUES ('2505', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:38:47');
INSERT INTO `bf_syslog` VALUES ('2506', 'admin', '退出', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:logout,[参数]:', '127.0.0.1', '2016-11-23 17:38:51');
INSERT INTO `bf_syslog` VALUES ('2507', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:38:55');
INSERT INTO `bf_syslog` VALUES ('2508', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:38:57');
INSERT INTO `bf_syslog` VALUES ('2509', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:38:57');
INSERT INTO `bf_syslog` VALUES ('2510', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:38:59');
INSERT INTO `bf_syslog` VALUES ('2511', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:39:24');
INSERT INTO `bf_syslog` VALUES ('2512', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:39:27');
INSERT INTO `bf_syslog` VALUES ('2513', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:39:51');
INSERT INTO `bf_syslog` VALUES ('2514', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:39:55');
INSERT INTO `bf_syslog` VALUES ('2515', 'admin', '日志管理-导出CSV', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogCSVExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:39:56');
INSERT INTO `bf_syslog` VALUES ('2516', 'admin', '日志管理-导出CSV', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogCSVExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:40:26');
INSERT INTO `bf_syslog` VALUES ('2517', 'admin', '日志管理-导出CSV', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogCSVExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:40:32');
INSERT INTO `bf_syslog` VALUES ('2518', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:41:40');
INSERT INTO `bf_syslog` VALUES ('2519', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:41:42');
INSERT INTO `bf_syslog` VALUES ('2520', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:41:42');
INSERT INTO `bf_syslog` VALUES ('2521', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:41:48');
INSERT INTO `bf_syslog` VALUES ('2522', 'admin', '日志管理-导出CSV', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogCSVExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:41:51');
INSERT INTO `bf_syslog` VALUES ('2523', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:null', '127.0.0.1', '2016-11-23 17:43:03');
INSERT INTO `bf_syslog` VALUES ('2524', null, '登录', '[类名]:com.xiaowu.web.controller.LoginController,[方法]:login,[参数]:loginname=admin&password=1&', '127.0.0.1', '2016-11-23 17:43:06');
INSERT INTO `bf_syslog` VALUES ('2525', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:43:06');
INSERT INTO `bf_syslog` VALUES ('2526', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:43:09');
INSERT INTO `bf_syslog` VALUES ('2527', 'admin', '日志管理-导出CSV', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogCSVExport,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:43:13');
INSERT INTO `bf_syslog` VALUES ('2528', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:44:50');
INSERT INTO `bf_syslog` VALUES ('2529', 'admin', '首页', '[类名]:com.xiaowu.web.controller.HomeController,[方法]:index,[参数]:null', '127.0.0.1', '2016-11-23 17:44:56');
INSERT INTO `bf_syslog` VALUES ('2530', 'admin', '日志管理-查看', '[类名]:com.xiaowu.web.controller.report.SysLogController,[方法]:sysLogData,[参数]:page=1&loginName=&roleName=&clientIp=&', '127.0.0.1', '2016-11-23 17:45:03');

-- ----------------------------
-- Table structure for bf_user
-- ----------------------------
DROP TABLE IF EXISTS `bf_user`;
CREATE TABLE `bf_user` (
  `id` int(19) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(64) NOT NULL COMMENT '登录账号',
  `name` varchar(64) NOT NULL COMMENT '姓名',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `salt` varchar(64) DEFAULT NULL COMMENT '盐（加密需用到）',
  `sex` int(2) NOT NULL DEFAULT '0' COMMENT '性别（0男，1女）',
  `age` int(2) DEFAULT '0' COMMENT '年龄',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '状态（0正常，1禁用）',
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `createdate` datetime NOT NULL COMMENT '创建时间',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of bf_user
-- ----------------------------
INSERT INTO `bf_user` VALUES ('1', 'admin', 'admin', '5741e53019c1bad96787458211d34cce', 'feff7e3a1bce1987cb54dec4b9d12020', '0', '25', '0', '1', '2015-12-06 13:14:05', '18707173376');
