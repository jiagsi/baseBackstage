<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link href="${staticPath}/static/plugIn/h-ui/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="${staticPath}/static/plugIn/h-ui/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
<link href="${staticPath}/static/plugIn/h-ui/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
<link href="${staticPath}/static/plugIn/Hui-iconfont/1.0.7/iconfont.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>赚金宝-后台登录 - v1.0</title>
<style type="text/css">
.loginBox{
	height: 260px
}
.headerInfo{
	height: 60px;
	background-color: #426374 ;
	position: fixed;
	top: 0;
	z-index: 9999;
	width: 100%;
	line-height: 60px;
	padding-left: 30px;
	color: #FFFFFF;
	font-size: 30px;
	font-family: "幼圆";
	letter-spacing:2px;
}
</style>
</head>

<body>
<div class="headerInfo">
		赚金宝后台管理 v1.0
</div>
<div class="loginWraper">
  <div id="loginform" class="loginBox">
    <form class="form form-horizontal" action="login" method="post">
      <div class="row cl">
      	<div class="formControls col-xs-11 text-c">
      		${error}
        </div>
      </div>
      <div class="row cl">
        <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
        <div class="formControls col-xs-8">
          <input id="" name="loginname" type="text" placeholder="账户" class="input-text size-L">
        </div>
      </div>
      <div class="row cl">
        <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
        <div class="formControls col-xs-8">
          <input id="" name="password" type="password" placeholder="密码" class="input-text size-L">
        </div>
      </div>
      <div class="row cl">
        <div class="formControls col-xs-8 col-xs-offset-3">
          <input name="" type="submit" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
          <input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
        </div>
      </div>
    </form>
  </div>
</div>
<div class="footer">京ICP备15065054号 Copyright ©2016 gaingold.cn, All Rights Reserved.</div>
</body>
</html>