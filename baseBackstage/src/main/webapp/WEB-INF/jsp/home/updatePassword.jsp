<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>角色管理- 添加</title>
</head>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="saveForm">
	
		<div class="row cl">
			<label class="form-label col-xs-3">旧密码：</label>
			<div class="formControls col-xs-5">
				<input type="text" class="input-text" id="formerPassword" name="password" autocomplete="off" datatype="*" ajaxurl="${staticPath}/home/checkAffirmPassword" placeholder="请输入新密码" nullmsg="请输入旧密码！" />
			</div>
			<div class="col-xs-4"> </div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-3">密码：</label>
			<div class="formControls col-xs-5">
				<input type="text" class="input-text" name="updatePassword" autocomplete="off" placeholder="请输入新密码" datatype="*" nullmsg="请输入密码！"  />
			</div>
			<div class="col-xs-4"> </div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-3">确认密码：</label>
			<div class="formControls col-xs-5">
				<input type="text" class="input-text" name="affirmPassword" autocomplete="off" datatype="*" recheck="updatePassword" nullmsg="请再输入一次密码！" errormsg="您两次输入的账号密码不一致！" />
			</div>
			<div class="col-xs-4"> </div>
		</div>
		
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
			</div>
		</div>
	</form>
</article>
<script type="text/javascript">

var index = parent.layer.getFrameIndex(window.name);

$().ready(function() {
	$("#saveForm").Validform({
		tiptype:2,
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			ajax({
				url : "${staticPath}/home/updatePassword",
				data : $("#saveForm").serializeArray(),
				success:function(data){
					if(data=='1'){
						parent.layerTopMsg("操作成功！");
						parent.layer.close(index);
					}else{
						layer.msg('操作失败，请稍后再试！');
					}
				}
			});
			return false;
		}
	});
});

</script>
</body>
</html>