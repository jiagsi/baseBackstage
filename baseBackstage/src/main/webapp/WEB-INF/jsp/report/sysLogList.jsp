<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>日志管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 系统管理<span class="c-gray en">&gt;</span> 角色管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<iframe name="name_iframe" id="id_iframe" class="hide"></iframe>
	<form action="${staticPath}/sysLog/sysLogExcelExport" method="post" id="queryForm" target="name_iframe">
	<input type="hidden" id="page" name="page"/>
	<div class="text-c"> 
		 &nbsp;登录名：<input type="text" name="loginName" id="" placeholder=" 请输入登录名" style="width:250px" class="input-text" value="">
		 &nbsp;操作名称：<input type="text" name="roleName" id="" placeholder=" 请输入操作名称" style="width:250px" class="input-text" value="">
		 &nbsp;IP：<input type="text" name="clientIp" id="" placeholder=" 请输入IP" style="width:250px" class="input-text" value="">
		<button name="" id="" class="btn btn-success" type="button" onclick="loadData(1)"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
	</div>
	</form>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> 
		<span class="l">
			<a class="btn btn-primary radius" onclick="exportLog('excel')" href="javascript:;"><i class="Hui-iconfont">&#xe644;</i> 导出Excel</a>
			<a class="btn btn-primary radius" onclick="exportLog('CVS')" href="javascript:;"><i class="Hui-iconfont">&#xe644;</i> 导出CVS</a>
		</span> 
	</div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
				<tr class="text-c">
					<th width="25"></th>
					<th width="80">登录名</th>
					<th width="80">操作名称</th>
					<th width="80">操作详细</th>
					<th width="80">IP</th>
					<th width="80">操作时间</th>
 				</tr>
			</thead>
			<tbody id="listData">
				<c:set var="listNumber" value="0"></c:set>
				<c:forEach items="${pageInfo.list}" var="list" varStatus="ind">
					<c:set var="listNumber" value="${ind.index+1}"></c:set>
					<tr class="text-c">
						<td>${ind.index+1}</td>
						<td>${list.loginName}</td>
						<td>${list.roleName}</td>
						<td>${list.optContent}</td>
						<td>${list.clientIp}</td>
						<td><fmt:formatDate value="${list.createTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
				</c:forEach>
				<c:if test="${listNumber==0}">
				<tr><td colspan="11" class="text-c c-warning">暂无数据</td></tr>
				</c:if>
				<tr>
					<td colspan="11">
						<div class="f-l lh-30" >共有数据：<b>${pageInfo.total}</b> 条 ,当前页：<b id="pageNum">${pageInfo.pageNum}</b> 页 ,每页显示：<b>${pageInfo.pageSize}</b> 条</div>
						<div class="f-r" id="paddingInfo">${pageInfo.pages}</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">

//分页加载方法
function loadData(page){
	
	$("#page").val(page);
	crud.pagingList({
		url : "${staticPath}/sysLog/sysLogData",	//访问URL
		data : $("#queryForm").serializeArray(),			//传输参数
		paddingId : "paddingInfo",							//分页DIV的ID
		tbodyId : "listData"								//分页tbody的DIV
	});

}

//默认加载第一页
$(function(){
	loadData(1);
});

function exportLog(type){
	if("excel"==type){
		$("#queryForm").attr("action","${staticPath}/sysLog/sysLogExcelExport");
		exportData('excel');
	}else{
		$("#queryForm").attr("action","${staticPath}/sysLog/sysLogCSVExport");
		exportData('cvs');
	}
}


</script> 
</body>
</html>