<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>赚金宝 v.1</title>
</head>
<body>
<header class="navbar-wrapper">
	<div class="navbar navbar-fixed-top">
		<div class="container-fluid cl"> <a class="logo navbar-logo f-l mr-10 hidden-xs" >赚金宝</a> <a class="logo navbar-logo-m f-l mr-10 visible-xs" >gaingold</a> <span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span> <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
			<nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
				<ul class="cl">
					<li id="Hui-msg" > <i class="Hui-iconfont" style="font-size:18px">&#xe60d;</i>&nbsp;<shiro:principal property="username"/>&nbsp;<span class="pipe">|</span></li>
					<shiro:hasPermission name="/home/updatePassword">
					<li id="Hui-msg" > <a href="javascript:layer_show('修改密码','${staticPath}/home/updatePasswordPage','600','300')" title="修改密码"><i class="Hui-iconfont" style="font-size:18px">&#xe605;</i>&nbsp;修改密码</a> <span class="pipe">|</span></li>
					</shiro:hasPermission>
					<li id="Hui-msg" > <a href="javascript:exit()" title="退出"><i class="Hui-iconfont" style="font-size:18px">&#xe706;</i>&nbsp;退出</a> </li>
					<li id="Hui-msg" style="display: none"> <a href="#" title="消息"><span class="badge badge-danger">1</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a> </li>
					<li id="Hui-skin" class="dropDown right dropDown_hover" style="display: none"> <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
							<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
							<li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
							<li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
							<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
							<li><a href="javascript:;" data-val="orange" title="绿色">橙色</a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>
<aside class="Hui-aside">
	<input runat="server" id="divScrollValue" type="hidden" value="" />
	<div class="menu_dropdown bk_2">
		<c:forEach var="menu" items="${menus}">
			<c:if test="${menu.pid==0}">
				<dl id="menu-article">
					<dt><i class="Hui-iconfont">&#xe616;</i> ${menu.name}<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
					<dd>
						<ul>
							<c:forEach var="menu1" items="${menus}">
								<c:if test="${menu.id==menu1.pid}">
									<li><a _href="${staticPath}${menu1.url}" data-title="${menu1.name}" href="javascript:void(0)">${menu1.name}</a></li>
								</c:if>
							</c:forEach>
						</ul>
					</dd>
				</dl>
			</c:if>
		</c:forEach>
	</div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<section class="Hui-article-box">
	<div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
		<div class="Hui-tabNav-wp">
			<ul id="min_title_list" class="acrossTab cl">
				<li class="active"><span title="我的桌面" data-href="welcome.jsp">我的桌面</span><em></em></li>
			</ul>
		</div>
		<div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a></div>
	</div>
	<div id="iframe_box" class="Hui-article">
		<div class="show_iframe">
			<div style="display:none" class="loading"></div>
			<iframe scrolling="yes" frameborder="0" src="${staticPath}/welcome.jsp"></iframe>
		</div>
	</div>
</section>
<script type="text/javascript">
function exit(){
	layer.confirm('是否确认退出系统', {
		  btn: ['确认','取消'] //按钮
	}, function(index){
		window.location.href="${staticPath}/home/exit";
	});	
}

</script>
</body>
</html>