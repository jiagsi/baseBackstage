<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>资源管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 系统管理 <span class="c-gray en">&gt;</span> 资源管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>


<div class="row cl">
	<div class="col-xs-2 col-sm-2 ">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
	<div class="col-xs-10 col-sm-10">
		<div class="panel panel-default">
			<div class="panel-header">资源列表</div>
			<div class="panel-body">
				<div class="page-container">
					<form action="" id="queryForm">
					<input type="hidden" id="page" name="page"/>
					<input type="hidden" name="pid" id="pid" value="0" />
					<div class="text-c"> 
						</span> 日期范围：
						<!-- <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}'})" id="logmin"　name="startDate" class="input-text Wdate" style="width:120px;"　placeholder="开始日期" />
						 -->
						 <input type="text" name="startTime" class="input-text Wdate" style="width:120px;" id="logmin" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}'})" />
						 -
						<input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d'})" id="logmax" name="endTime" class="input-text Wdate" style="width:120px;"　placeholder="结束日期"　/>
						<button name="" id="" class="btn btn-success" type="button" onclick="loadData(1)"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
					</div>
					</form>
					<div class="cl pd-5 bg-1 bk-gray mt-20"> 
					<span class="l">
					<a class="btn btn-primary radius" onclick="layer_show('添加','${staticPath}/resource/resourceEditPage','660','500')" href="javascript:;"><i class="Hui-iconfont"></i> 添加资源</a>
					</span> 
					</div>
					<div class="mt-20">
						<table class="table table-border table-bordered table-bg table-hover table-sort">
							<thead>
								<tr class="text-c">
									<th width="25"></th>
									<th width="80">资源名称</th>
									<th width="80">资源路径</th>
									<th width="120">排序</th>
									<th width="75">图标</th>
									<th width="60">资源类型</th>
									<th width="120">状态</th>
									<th width="120">描述</th>
									<th width="120">操作</th>
				 				</tr>
							</thead>
							<tbody id="listData">
								<c:set var="listNumber" value="0"></c:set>
								<c:forEach items="${pageInfo.list}" var="list" varStatus="ind" >
									<c:set var="listNumber" value="${ind.index+1}"></c:set>
									<tr class="text-c">
										<td>${ind.index+1}</td>
										<td>${list.name} </td>
										<td>${list.url}</td>
										<td>${list.seq}</td>
										<td><i class="Hui-iconfont">${list.icon}</i></td>
										<td>
											<c:if test="${list.resourcetype==0}">菜单</c:if>
											<c:if test="${list.resourcetype==1}">功能</c:if>
										</td>
										<td>
										<c:if test="${list.status==0}"><span class="label label-success radius">正常</span></c:if>
										<c:if test="${list.status==1}"><span class="label label-defaunt radius">禁用</span></c:if></td>
										<td>${list.description}</td>
										<td>
											<a style="text-decoration:none" class="ml-5" onclick="layer_show('添加','${staticPath}/resource/resourceEditPage?id=${list.id}','660','500')" href="javascript:;" title="编辑"><i class="Hui-iconfont"></i>&nbsp;修改</a>
											<a style="text-decoration:none" class="ml-5" onclick="del(${list.id})" href="javascript:;" title="删除"><i class="Hui-iconfont"></i>&nbsp;删除</a>
										</td>
									</tr>
								</c:forEach>
								<c:if test="${listNumber==0}">
									<tr><td colspan="10" class="text-c c-warning">暂无数据</td></tr>
								</c:if>
								<tr>
									<td colspan="9">
										<div class="f-l lh-30" >共有数据：<b>${pageInfo.total}</b> 条 ,当前页：<b id="pageNum">${pageInfo.pageNum}</b> 页 ,每页显示：<b>${pageInfo.pageSize}</b> 条</div>
										<div class="f-r" id="paddingInfo">${pageInfo.pages}</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

//分页加载方法
function loadData(page){
	
	$("#page").val(page);
	crud.pagingList({
		url : "${staticPath}/resource/resourceData",	//访问URL
		data : $("#queryForm").serializeArray(),			//传输参数
		paddingId : "paddingInfo",							//分页DIV的ID
		tbodyId : "listData"								//分页tbody的DIV
	});
}

//菜单点击
function zTreeOnClick(event, treeId, treeNode) {
    $("#pid").val(treeNode.id);
    loadData(1);
};

var zTreeObj;
// zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
var setting = {
	data: {
		simpleData: {
			enable: true,
			idKey: "id",
			pIdKey: "pId",
			rootPId: ""
		}
	},
	callback:{
			onClick:zTreeOnClick
	}
};
// zTree 的数据属性，深入使用请参考 API 文档（zTreeNode 节点数据详解）
var zNodes = [];


//得到菜单数据
function getMenuData(){
	ajax({
		type:"post",
		url:"${staticPath}/resource/resourceMenuData",
		data:{resourcetype:0},
		success:function(data){
			zNodes = [];
			for(var i=0;i<data.length;i++){
				zNodes.push({"id":data[i].id,"pId":data[i].pid,open:true,"name":data[i].name});
			}
			zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		}
	});
}


$(function(){
	//默认加载第一页
	loadData(1);
	//默认加载左边的菜单
	getMenuData();
});

/*添加修改页面*/
function member_show(title,url,w,h){
	layer_show(title,url,w,h);
}

//删除
function del(id){
	crud.del({
		url:"${staticPath}/resource/resourceDelete",
		data:{id:id},
		success:function(){
			loadData(1);
			getMenuData();
		}
	});
}

</script> 
</body>
</html>