<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>角色管理 - 授权</title>
<style type="text/css">
input {
	margin: 2px;
}
</style>
</head>
<body>

<div style="width:60%" class="f-l pt-10">
<ul id="treeDemo" class="ztree"></ul>
</div>
<div style="width:30%" class="f-r pt-10">
<input class="btn btn-primary size-S radius" type="button" value="授权" onclick="save()"><hr />
<input class="btn btn-default size-S radius" type="button" value="全选" onclick="checkAllNodes()" />
<input class="btn btn-default size-S radius" type="button" value="全取消" onclick="cancelAllNodes()" />
</div>
<script type="text/javascript">
var zTreeObj;
//zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
var setting = {
	data: {
		simpleData: {
			enable: true,
			idKey: "id",
			pIdKey: "pId",
			rootPId: ""
		}
	},
	check:{
		enable: true,
		chkboxType: { "Y": "p", "N": "s" }  //checkbox 勾选操作，只影响父级节点；取消勾选操作，只影响子级节
	},
	callback:{
	}
};
//zTree 的数据属性，深入使用请参考 API 文档（zTreeNode 节点数据详解）
var zNodes = [];

//得到资源数据
function getResourceData(){
	ajax({
		type:"post",
		url:"${staticPath}/role/resourceData",
		data:{id:"${id}"},
		success:function(data){
			zNodes = [];
			for(var i=0;i<data.length;i++){
				zNodes.push({"id":data[i].id,"pId":data[i].pid,open:true,"name":data[i].name,"checked":data[i].isResource});
			}
			zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		}
	});
}

$(function(){
	//默认加载资源数据
	getResourceData();
});

//全选
function checkAllNodes() {
    zTreeObj.checkAllNodes(true);
}
//全取消
function cancelAllNodes() {
    zTreeObj.checkAllNodes(false);
}

var index = parent.layer.getFrameIndex(window.name);
function save(){
	 nodes=zTreeObj.getCheckedNodes(true);
	 var v="";
	 for(var i=0;i<nodes.length;i++){
		 v += nodes[i].id+",";
	 }
	 ajax({
		 url:"${staticPath}/role/roleAuthorityOperation",
		 data:{id:"${id}",resources:v},
		 dataType:"text",
		 success:function(data){
			if(data=='success'){
				parent.layerTopMsg("授权成功");
				parent.layer.close(index);
			}else{
				layerTopMsg('授权失败，请稍后再试');
			}
		 }
	 });
}

</script>
</body>
</html>