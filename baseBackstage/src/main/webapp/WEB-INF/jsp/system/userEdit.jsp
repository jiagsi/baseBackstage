<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>用户管理- 添加</title>
</head>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="saveForm">
		<input type="hidden" name="id"  value="${user.id}" id="id"/>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>登录账号：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${user.loginname}" placeholder="" name="loginname" >
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>姓名：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${user.name}" placeholder="" name="name" >
				<%-- <span class="select-box">
				<select class="select" size="1" name="resourcetype">
					<option value="0" <c:if test="${resource.resourcetype==0}">selected</c:if> >菜单</option>
					<option value="1" <c:if test="${resource.resourcetype==1}">selected</c:if>>功能</option>
				</select>
				</span> --%>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>密码：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="" placeholder="" name="password">
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>性别：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<span class="select-box">
				<select class="select" size="1" name="sex">
					<option value="0" <c:if test="${user.sex==0}">selected</c:if> >男</option>
					<option value="1" <c:if test="${user.sex==1}">selected</c:if>>女</option>
				</select>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>年龄：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${user.age}" placeholder="" name="age">
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>状态：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<span class="select-box">
				<select class="select" size="1" name="status">
					<option value="0" <c:if test="${user.status==0}">selected</c:if> >启用</option>
					<option value="1" <c:if test="${user.status==1}">selected</c:if> >禁用</option>
				</select>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>手机号：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${user.phone}" placeholder="" name="phone">
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>角色：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<span class="select-box">
				<select class="select" size="1" name="roleId">
					<c:forEach items="${roles}" var="role">
						<option value="${role.id}" <c:if test="${user.roleId==role.id}">selected</c:if> >${role.name}</option>
					</c:forEach>
				</select>
				</span>
			</div>
		</div>
		
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="button" onclick="save()" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
			</div>
		</div>
	</form>
	<c:if test="${not empty role.id}">
		<div class="Huialert Huialert-info mt-10 radius" >
			<ol class="linenums">
				<li>需要修改密码则在密码框输入最新的密码，不修改则不需要输入</li>
			</ol>
		</div>
	</c:if>
</article>
<script type="text/javascript">
$(document).ready(function() {
  	$(".js-example-basic-single").select2();
});

var index = parent.layer.getFrameIndex(window.name);


function save(){
	var id  = $("#id").val();
	var url = "";
	if(id!=''){
		url = "${staticPath}/user/userUpdate";
	}else{
		url = "${staticPath}/user/userAdd";
	}
	ajax({
		url : url,
		data : $("#saveForm").serializeArray(),
		success:function(data){
			if(data=='1'){
				parent.loadData(1);
				parent.layerTopMsg("操作成功");
				parent.layer.close(index);
			}else{
				layer.msg('操作失败，请稍后再试！');
			}
		}
	});
	
}
</script>
</body>
</html>