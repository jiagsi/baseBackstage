<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>用户管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 系统管理<span class="c-gray en">&gt;</span> 用户管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<iframe name="name_iframe" id="id_iframe" class="hide"></iframe>
	<form action="${staticPath}/accountRegistration/accountRegistrationExcelExport" method="post" id="queryForm" target="name_iframe">
	<input type="hidden" id="page" name="page"/>
	<div class="text-c"> 
		状态：<span class="select-box inline">
		<select name="status" class="select" onchange="loadData(1)">
			<option value="">全部</option>
			<option value="0">正常</option>
			<option value="1">禁用</option>
		</select>
		</span>
		 &nbsp;角色名称：<input type="text" name="name" id="" placeholder=" 请输入角色名称" style="width:250px" class="input-text" value="">
		 &nbsp;描述：<input type="text" name="description" id="" placeholder=" 请输入描述" style="width:250px" class="input-text" value="">
		<button name="" id="" class="btn btn-success" type="button" onclick="loadData(1)"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
	</div>
	</form>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> 
		<span class="l">
			<a class="btn btn-primary radius" onclick="layer_show('添加','${staticPath}/user/userEditPage','660','500')" href="javascript:;"><i class="Hui-iconfont"></i> 添加用户</a>
		</span> 
	</div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-bg table-hover table-sort">
			<thead>
				<tr class="text-c">
					<th width="25"></th>
					<th width="80">登录账号</th>
					<th width="80">姓名</th>
					<th width="80">性别</th>
					<th width="80">年龄</th>
					<th width="80">手机</th>
					<th width="80">状态</th>
					<th width="80">角色</th>
					<th width="80">操作</th>
 				</tr>
			</thead>
			<tbody id="listData">
				<c:set var="listNumber" value="0"></c:set>
				<c:forEach items="${pageInfo.list}" var="list" varStatus="ind">
					<c:set var="listNumber" value="${ind.index+1}"></c:set>
					<tr class="text-c">
						<td>${ind.index+1}</td>
						<td>${list.loginname}</td>
						<td>${list.name}</td>
						<td>
							<c:if test="${list.sex==0}">男</c:if>
							<c:if test="${list.sex==1}">女</c:if>
						</td>
						<td>${list.age}</td>
						<td>${list.phone}</td>
						<td>
							<c:if test="${list.status==0}"><span class="label label-success radius">正常</span></c:if>
							<c:if test="${list.status==1}"><span class="label label-defaunt radius">禁用</span></c:if>
						</td>
						<td>${list.roleName}</td>
						<td>
							<a style="text-decoration:none" class="ml-5" onclick="layer_show('修改','${staticPath}/user/userEditPage?id=${list.id}','660','500')" href="javascript:;" title="编辑"><i class="Hui-iconfont"></i>&nbsp;修改</a>
							<a style="text-decoration:none" class="ml-5" onclick="del(${list.id})" href="javascript:;" title="删除"><i class="Hui-iconfont"></i>&nbsp;删除</a>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${listNumber==0}">
				<tr><td colspan="11" class="text-c c-warning">暂无数据</td></tr>
				</c:if>
				<tr>
					<td colspan="11">
						<div class="f-l lh-30" >共有数据：<b>${pageInfo.total}</b> 条 ,当前页：<b id="pageNum">${pageInfo.pageNum}</b> 页 ,每页显示：<b>${pageInfo.pageSize}</b> 条</div>
						<div class="f-r" id="paddingInfo">${pageInfo.pages}</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">

//分页加载方法
function loadData(page){
	
	$("#page").val(page);
	crud.pagingList({
		url : "${staticPath}/user/userData",	//访问URL
		data : $("#queryForm").serializeArray(),			//传输参数
		paddingId : "paddingInfo",							//分页DIV的ID
		tbodyId : "listData"								//分页tbody的DIV
	});

}

//默认加载第一页
$(function(){
	loadData(1);
});


//删除
function del(id){
	crud.del({
		url:"${staticPath}/user/userDelete",
		data:{id:id},
		dataType:"text",
		success:function(data){
			loadData(1);
		}
	});
}


</script> 
</body>
</html>