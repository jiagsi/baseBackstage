<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<%@ include file="/commons/basejs.jsp" %>
<title>资源管理 - 添加</title>
</head>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="saveForm">
		<input type="hidden" name="id"  value="${resource.id}" id="id"/>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>资源名称：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${resource.name}" placeholder="" name="name" >
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>资源类型：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<span class="select-box">
				<select class="select" size="1" name="resourcetype">
					<option value="0" <c:if test="${resource.resourcetype==0}">selected</c:if> >菜单</option>
					<option value="1" <c:if test="${resource.resourcetype==1}">selected</c:if>>功能</option>
				</select>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>排序：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${resource.seq}" placeholder="" name="seq">
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>菜单图标：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="${resource.icon}" placeholder="" name="icon">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>上级资源：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<select class="js-example-basic-single " name="pid" style="width: 100%">
					<option value="">请选择</option>
					<c:forEach items="${list}" var="resourceInfo">
				    	<option value="${resourceInfo.id}" <c:if test="${resource.pid==resourceInfo.id}">selected</c:if> >${resourceInfo.name}</option>
				    </c:forEach>
				</select>
			</div>
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>状态：</label>
			<div class="formControls col-xs-4 col-sm-4">
				<span class="select-box">
				<select class="select" size="1" name="status">
					<option value="0" <c:if test="${resource.status==0}">selected</c:if> >启用</option>
					<option value="1" <c:if test="${resource.status==1}">selected</c:if> >禁用</option>
				</select>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>资源路径：</label>
			<div class="formControls col-xs-10 col-sm-10">
				<input type="text" class="input-text" value="${resource.url}" placeholder="" name="url">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>描述：</label>
			<div class="formControls col-xs-10 col-sm-10">
				<input type="text" class="input-text" value="${resource.description}" placeholder="" name="description">
			</div>
		</div>
		
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="button" onclick="save()" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
			</div>
		</div>
	</form>
</article>
<script type="text/javascript">
$(document).ready(function() {
  	$(".js-example-basic-single").select2();
});

var index = parent.layer.getFrameIndex(window.name);


function save(){
	var id  = $("#id").val();
	var url = "";
	if(id!=''){
		url = "${staticPath}/resource/resourceUpdate";
	}else{
		url = "${staticPath}/resource/resourceAdd";
	}
	ajax({
		url : url,
		data : $("#saveForm").serializeArray(),
		success:function(data){
			if(data=='1'){
				parent.getMenuData();
				parent.loadData(1);
				parent.layer.close(index);
			}else{
				layer.msg('操作失败，请稍后再试！');
			}
		}
	});
	
	
}
</script>
</body>
</html>