/**
 * ajax请求封装
 * @param Obj
 *  url : 请求地址，必填
 *  success : 成功回调函数，必填
 *  type : 请求方式，不填默认post
 *  data : 请求参数，选填
 *  dataType : 返回数据类型，不填默认json
 *  error : 失败回调函数，选填
 */
function ajax(obj){
	 var type = "post";
	 if(typeof(obj.type)!='undefined'){
		 type = obj.type;
	 }
	 var data = {};
	 if(typeof(obj.data)!='undefined'){
		 data = obj.data;
	 }
	 var dataType = "json";
	 if(typeof(obj.dataType)!='undefined'){
		 dataType = obj.dataType;
	 }
	 $.ajax({
         type: type,
         url: obj.url,
         data: data,
         dataType:dataType,
         success: function(data){
        	 obj.success(data);
         },
         error:function(XMLHttpRequest, textStatus, errorThrown){
        	 if (typeof(obj.error) == "function") { 
        		 obj.error;
        	 }else{
        		 if(XMLHttpRequest.readyState==4){
        			 if(XMLHttpRequest.responseText.indexOf("无权操作")>0){
                		 //统一处理异常
                		 layer.msg('无权操作');
        			 }
        		 }else{
            		 //统一处理异常
            		 layer.msg('网络请求失败，请稍后再试！');
        		 }
        	 }
         }
     });
}

$(function(){
	//默认给ID为queryForm加上回车事件，按下回车查询第一页
	$("#queryForm").keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);  
	    if(keycode == '13'){  
	    	loadData(1);
	    }  
	});
});


//分页
function loadPadding(paddingId){
	laypage({
	    cont: paddingId,
	    pages: $("#"+paddingId).html(),
	    curr: $("#pageNum").html(),
	    skin: '#c00',
	    groups:8,
	    jump: function(obj,first){
	        if(!first){
	        	loadData(obj.curr);
	        }
	    }
	});
}

//增加(Create)、读取(Retrieve)、更新(Update)、删除(Delete)
var crud = {
		/**
		 * 分页数据
		 * url:访问地址，必填
		 * data:查询参数,必填
		 * paddingId:分页div的Id,不需要分页则不需要填
		 * tbodyId:数据位置的tbody的ID
		 * @param obj
		 */
		pagingList:function(obj){
			//默认tbody的ID
			var defaultTbodyId = "listData";
	 		if(typeof(obj.divId)!='undefined'){
	 			defaultTbodyId = obj.tbodyId;
	 		}
	 	    var now=Date.now();
	 		//弹出层
	 		var layerLoad =layer.load(0,{shade: 0.1});
	 		$("#"+defaultTbodyId).load(obj.url+" #"+defaultTbodyId+" tr",obj.data,function(response,status,xhr){
	 			if(response.indexOf("userNotLogin")>0){
	 				//刷新父页面
	 				parent.location.reload();
	 			}
	 			//console.log(response);
	 			//console.log(status);
	 			//console.log(xhr);
	 			if(response.indexOf("NoPermissions")>0){
	 				layer.close(layerLoad);
	 				layerMsg("没有权限");
	 			}else{
	 				if (typeof(obj.success) == "function") { 
		 				obj.success();
		 			}else{
		 				//不需要分页插件，则不需要传入paddingId
		 				if(typeof(obj.paddingId)!='undefined'){
		 					//加载分页插件
				 			loadPadding(obj.paddingId);
		 				}
			 		
			 			//为了保证加载动画不闪屏，设置动画最少时间
			 			
			 			//最少延迟时间
			 			var delayTime = 300;
			 			//运行时间
			 			var runningTime = new Date()-now;
			 			if(runningTime > delayTime){
			 				layer.close(layerLoad);
			 			}else{
			 				setTimeout(function(){
				 				layer.close(layerLoad);
			 				},(delayTime - runningTime));
			 			}
		 			}
	 			}
	 		});
		},
		del:function(obj){
			layer.confirm('是否确认删除!', {
				  btn: ['确认','取消'] //按钮
			}, function(index){
				ajax({
					url : obj.url,
					data : obj.data,
					success:function(data){
						if(data > 0){
							layer.msg('操作成功', {offset: 0, shift: 6});
							if (typeof(obj.success) == "function") { 
								obj.success();
							}
						}else{
							layer.msg('操作失败，请稍后再试！');
						}
					}
				});
			});
		}
};



//导出excel
function exportData(content){
	layer.confirm('是否确认导出'+content, {
		  btn: ['确认','取消'] //按钮
	}, function(index){
		//提示
		layer.load(0,{shade: 0.1,time: 1000 });//1秒=1000毫秒
		$("#queryForm").submit();
		layer.close(index);
	});
};

//打印数据
function printData(){
	$(".page-container").jqprint();
}

//全屏显示详情
function showdDetails(obj){
	var btnContent = ['确认','取消'];
	if(typeof(obj.btnContent)== 'object' ){
		btnContent = obj.btnContent;
	}
	var index = layer.open({
		type: 2,
		title: obj.title,
		content: obj.url,
		btn:btnContent,
		yes:function(index,layero){
			if (typeof(obj.yes) == "function") { 
				obj.yes(index,layero);
			}else{
				layer.close(index); 
			}
		},
		cancel:function(){
			if (typeof(obj.cancel) == "function") { 
				obj.cancel();
			}
		}
	});
	layer.full(index);
}

//正上方提示
function layerTopMsg(content){
	//正上方,提示
	layer.msg(content, {
	  offset: 0,
	  shift: 6
	});
}

//正中间提示,毫秒
function layerMsg(content,time){
	//正中间,提示
	if(typeof(time)=='undefined'){
		//msg 默认是3000毫秒，如果不填写则代表默认
		time = 3000;
	}
	//正中间,提示
	layer.msg(content,{
		shade: 0.1,
		time : time
	});
}



//全屏页面显示
function layer_full(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
