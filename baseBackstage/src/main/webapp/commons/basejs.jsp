<%--标签 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="edge" />

<!--[if lt IE 9]>
<script type="text/javascript" src="${staticPath }/static/plugIn/compatible/html5.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn/compatible/respond.min.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn/compatible/PIE_IE678.js"></script>
<![endif]-->
<!-- cs -->
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/h-ui/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/h-ui/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/h-ui/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/h-ui/h-ui.admin/css/style.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/zTree/v3/css/zTreeStyle/zTreeStyle.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/select2/select2.css" />
<link rel="stylesheet" type="text/css" href="${staticPath }/static/plugIn/zyupload/skins/zyupload-1.0.0.min.css"/>
<link rel="stylesheet" type="text/css" href="${staticPath }/static/css/commons.css" />

<!--[if lt IE 9]>
<link href="${staticPath }/static/plugIn/h-ui/h-ui/css/H-ui.ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- js -->
<script type="text/javascript">
    var basePath = "${staticPath }";
</script>
<script type="text/javascript" src="${staticPath }/static/plugIn/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/layer/2.1/layer.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/My97DatePicker/WdatePicker.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/h-ui/h-ui/js/H-ui.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/h-ui/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn/laypage-v1.3/laypage/laypage.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn/eCharts/echarts.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/jquery.jqprint/jquery-migrate-1.1.0.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/jquery.jqprint/jquery.jqprint-0.3.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/select2/select2.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn/select2/select2_locale_zh-CN.js"></script> 
<script type="text/javascript" src="${staticPath }/commons/common.js"></script>
<script type="text/javascript" src="${staticPath }/static/plugIn//Validform/5.3.2/Validform.min.js"></script> 
<script type="text/javascript" src="${staticPath }/static/plugIn//Validform/5.3.2/passwordStrength-min.js"></script> 

<script type="text/javascript" src="${staticPath }/static/plugIn/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="${staticPath }/static/plugIn/zyupload/zyupload-1.0.0.min.js"></script>
<%-- <script type="text/javascript" src="${staticPath }/static/plugIn/zyupload/zyupload.basic-1.0.0.min.js"></script> --%>



