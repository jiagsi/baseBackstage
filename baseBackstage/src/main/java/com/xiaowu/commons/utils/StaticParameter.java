package com.xiaowu.commons.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 静态参数
 * @author WH
 *
 */
public class StaticParameter {

	//资源状态-菜单
	public final static int ResourceStatusMenu = 0;
	//资源状态-功能
	public final static int ResourceStatusFunctionu = 1;
	
	//导出最大数限制
	public final static int exportMaximumNumber = 500000;
	
	//状态 - 启用
	public final static int statusEnable = 0;
	//状态 - 禁用
	public final static int statusDisable = 1;
	
	
}
