package com.xiaowu.commons.utils;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;

/**
 * 公用 工具类
 * @author WH
 *
 */
public class CommonUtils {

	/**
	 * 得到UUID
	 * @return
	 */
	public static String getUUID(){
		 UUID uuid = UUID.randomUUID();
		 return uuid.toString().replaceAll("-","");
	}
	
	/**
	 * 得到shiro自带盐
	 * @return
	 */
	public static String getSalt(){
		String salt = new SecureRandomNumberGenerator().nextBytes().toHex();//shiro自带盐生成器
		return  salt;
	}
	
	 /**
	 * 通过HttpServletRequest返回IP地址
	 * @param request HttpServletRequest
	 * @return ip String
	 * @throws Exception
	 */
	public static String getIpAddr(HttpServletRequest request) throws Exception {
	    String ip = request.getHeader("x-forwarded-for");
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("HTTP_CLIENT_IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getRemoteAddr();
	    }
	    return ip;
	}

	
}
