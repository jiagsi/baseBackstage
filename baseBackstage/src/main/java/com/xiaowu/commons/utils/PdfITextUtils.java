package com.xiaowu.commons.utils;

import java.io.IOException;
import java.io.OutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactoryImp;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * itext导出pdf
 * @author WH
 *
 */
public class PdfITextUtils {

	
	private int fontSize = 12;
	private int style = Font.UNDEFINED;
	private int horizontalAlignment = Element.ALIGN_LEFT;
	private float fixedHeight = 20f;
	
	public PdfITextUtils(int fontSize,int style,int horizontalAlignment,float fixedHeight) {
		this.fontSize=fontSize;
		this.style=style;
		this.horizontalAlignment=horizontalAlignment;
		this.fixedHeight=fixedHeight;
	}

    public final String CHARACTOR_FONT_CH_FILE = "font/SIMHEI.TTF"; 
    
    public Document createPdf(OutputStream os) throws DocumentException{
    	// step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document,os);
        // step 3
        document.open();
        
        return document;
    }
    
    /**
     * 得到 PdfPCell
     * @param title 内容
     * @return
     */
    public PdfPCell getPdfPCell(String title){
    	Paragraph content = new Paragraph(title,getBlackFont(fontSize,style));
    	PdfPCell cell = new PdfPCell(content);
    	cell.setFixedHeight(fixedHeight);
        cell.setHorizontalAlignment(horizontalAlignment);
        cell.setUseAscender(true); 
        cell.setVerticalAlignment(cell.ALIGN_MIDDLE); 
        return cell;
    }
    
    /**
     * 得到 PdfPCell
     * @param title 内容
     * @param fontSize	字体大小
     * @param style	样式，加粗Font.BOLD，
     * @param horizontalAlignment	靠左 Element.ALIGN_LEFT,居中Element.ALIGN_CENTER,靠右Element.ALIGN_RIGHT
     * @param fixedHeight	高度
     * @return
     */
    public PdfPCell getPdfPCell(String title,int fontSize,int style,int horizontalAlignment,float fixedHeight){
    	Paragraph content = new Paragraph(title,getBlackFont(fontSize,style));
    	PdfPCell cell = new PdfPCell(content);
    	cell.setFixedHeight(fixedHeight);
        cell.setHorizontalAlignment(horizontalAlignment);
        cell.setUseAscender(true); 
        cell.setVerticalAlignment(cell.ALIGN_MIDDLE); 
        return cell;
    }

    /**
     * 得到 PdfPCell
     * @param title 内容
     * @param colspan 合并列
     * @param fontSize 字体大小
     * @param style 样式，加粗Font.BOLD，
     * @param horizontalAlignment 靠左 Element.ALIGN_LEFT,居中Element.ALIGN_CENTER,靠右Element.ALIGN_RIGHT
     * @param fixedHeight 高度
     * @return
     */
    public PdfPCell getPdfPCell(String title,Integer colspan,int fontSize,int style,int horizontalAlignment,float fixedHeight){
    	Paragraph content = new Paragraph(title,getBlackFont(fontSize,style));
    	PdfPCell cell = new PdfPCell(content);
    	cell.setFixedHeight(fixedHeight);
        cell.setHorizontalAlignment(horizontalAlignment);
        cell.setUseAscender(true); 
        cell.setVerticalAlignment(cell.ALIGN_MIDDLE); 
        cell.setColspan(colspan);
        return cell;
    }
   
    
    
    /**
     * 得到黑色字体
     * @param fontSize
     * @return
     */
    public Font getBlackFont(int fontSize,int style){
    	//Font.UNDEFINED
    	 Font font = createCHineseFont(fontSize,style, null);
	     return font;
    }
   
    
    
    /** 
     * 功能： 返回支持中文的字体---仿宋 
     * @param size 字体大小 
     * @param style 字体风格 
     * @param color 字体 颜色 
     * @return  字体格式 
     */  
    public Font createCHineseFont(float size, int style, BaseColor color) {  
        BaseFont bfChinese = null;  
        try {  
            bfChinese = BaseFont.createFont(CHARACTOR_FONT_CH_FILE,BaseFont.IDENTITY_H, BaseFont.EMBEDDED);  
        } catch (DocumentException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        Font font = new Font(bfChinese);
        font.setSize(size);
        font.setStyle(style);
        font.setColor(color);
        return font;
    } 
}
