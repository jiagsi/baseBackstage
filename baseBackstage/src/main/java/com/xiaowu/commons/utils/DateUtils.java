package com.xiaowu.commons.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.format.datetime.joda.DateTimeParser;

/**
 * 日期工具类
 * @author WH
 *
 */
public class DateUtils {
	
    static final String date_formatPattern = "yyyy-MM-dd";  
    
    static final String time_formatPattern = "HH:mm:ss";  

    static final String date_time_formatPattern = "yyyy-MM-dd HH:mm:ss";  
    
    /**
     * 获取当前的日期
     * @param formatPattern 格式
     * @return
     */
    public static String getCurren(String formatPattern){
    	 SimpleDateFormat format = new SimpleDateFormat(formatPattern);        
         return format.format(new Date());  
    }
    
    /** 
     * 获取当前日期 (yyyy-MM-dd)
     * @return 
     */  
    public static String getCurrentDate(){  
        SimpleDateFormat format = new SimpleDateFormat(date_formatPattern);        
        return format.format(new Date());  
    }  
    
    /**
     * 获取当前时间(HH:mm:ss)
     * @return
     */
    public static String getCurrentTime(){
    	SimpleDateFormat format = new SimpleDateFormat(time_formatPattern);        
        return format.format(new Date());  
    }
    
    /**
     * 获取当前的日期时间(yyyy-MM-dd HH:mm:ss)
     * @return
     */
    public static String getCurrentDateTime(){
    	SimpleDateFormat format = new SimpleDateFormat(date_time_formatPattern);        
        return format.format(new Date());  
    }
  
    /** 
     * 日期转换成字符串 (yyyy-MM-dd)
     * @param date 
     * @return 
     */  
    public static String dateToString(Date date){  
        SimpleDateFormat format = new SimpleDateFormat(date_formatPattern);  
        return format.format(date);  
    }
    
    /** 
     * 时间转换成字符串 (HH:mm:ss)
     * @param date 
     * @return 
     */  
    public static String timeToString(Date date){  
        SimpleDateFormat format = new SimpleDateFormat(time_formatPattern);  
        return format.format(date);  
    }
    
    /** 
     * 时间日期转换成字符串 (yyyy-MM-dd HH:mm:ss)
     * @param date 
     * @return 
     */  
    public static String dateTimeToString(Date date){  
        SimpleDateFormat format = new SimpleDateFormat(date_time_formatPattern);  
        return format.format(date);  
    }
    
    /**
     * 日期转换为字符串(yyyy-MM-dd HH:mm:ss)
     * @param date
     * @param formatPattern
     * @return
     */
    public static String DatetoString(Date date,String formatPattern){  
        SimpleDateFormat format = new SimpleDateFormat(date_time_formatPattern);  
        return format.format(date);  
    }
    
    /** 
     * 字符串转换日期 (yyyy-MM-dd)
     * @param str 
     * @return 
     */  
    public static Date stringToDate(String str){  
        SimpleDateFormat format = new SimpleDateFormat(date_formatPattern);  
        if(!str.equals("")&&str!=null){  
            try {  
                return format.parse(str);  
            } catch (ParseException e) {  
                e.printStackTrace();  
            }  
        }  
        return null;  
    }
    
    /** 
     * 字符串转换时间(HH:mm:ss)
     * @param str 
     * @return 
     */  
    public static Date stringToTime(String str){  
        SimpleDateFormat format = new SimpleDateFormat(time_formatPattern);  
        if(!str.equals("")&&str!=null){  
            try {  
                return format.parse(str);  
            } catch (ParseException e) {  
                e.printStackTrace();  
            }  
        }  
        return null;  
    }
    
    /** 
     * 字符串转换日期时间(yyyy-MM-dd HH:mm:ss)
     * @param str 
     * @return 
     */  
    public static Date stringToDateTime(String str){  
        SimpleDateFormat format = new SimpleDateFormat(date_time_formatPattern);  
        if(!str.equals("")&&str!=null){  
            try {  
                return format.parse(str);  
            } catch (ParseException e) {  
                e.printStackTrace();  
            }  
        }  
        return null;  
    }
    
    /** 
     * 字符串转换日期
     * @param str 
     * @return 
     */  
    public static Date stringToDate(String str,String formatPattern){  
        SimpleDateFormat format = new SimpleDateFormat(formatPattern);  
        if(!str.equals("")&&str!=null){  
            try {  
                return format.parse(str);  
            } catch (ParseException e) {  
                e.printStackTrace();  
            }  
        }  
        return null;  
    }
    
    /**
     * 获取当天的前后日期
     * @param day	传入负数获取往前的日期，正数为往后的日期
     * @return
     */
    public final static String dateChange(int day){
    	Calendar   cal   =   Calendar.getInstance();
    	cal.add(Calendar.DATE,   day);
    	return new SimpleDateFormat( "yyyy-MM-dd").format(cal.getTime());
    }
    /**
     * 获取当天的前后日期带时分秒
     * @param day
     * @return
     */
    public final static String dateChangeHms( int day){
    	 Calendar now = Calendar.getInstance();   
         now.set(Calendar.DATE, now.get(Calendar.DATE) + day);   
         return  new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss").format(now.getTime()); 
    }
    
	/**
	 *	获取当天的前后日期
     * @param day	传入负数获取往前的日期，正数为往后的日期
	 * @param format	传入时间格式
	 * @return
	 */
    public final static String dateChange(int day,String format){
    	Calendar   cal   =   Calendar.getInstance();
    	cal.add(Calendar.DATE,   day);
    	String yesterday = new SimpleDateFormat(format).format(cal.getTime());
    	return yesterday;
    }
    
    
    /**
     * 获取传入时间的前后时间
     * @param d		时间
     * @param day	前后天数
     * @param format	返回时间格式
     * @return
     */
    public final static String dateChangeByDate(Date d,int day,String format){
    	Calendar   cal   =   Calendar.getInstance();
    	cal.setTime(d);   
    	cal.add(Calendar.DATE,   day);
    	String yesterday = new SimpleDateFormat(format).format(cal.getTime());
    	return yesterday;
    }
    
    /**
     * 转换为指定格式的时间字符串
     * @param str	需转换格式的字符串
     * @param format	时间格式
     * @return
     */
    public final static String stringToSDF(String str,String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date=null;
		try {
			date = sdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String newTime = sdf.format(date);
    	return newTime;
    }
    
    public static void main(String[] args) {
//		System.out.println(DateUtils.dateChange(-1));
		System.err.println(DateUtils.stringToSDF("2016-07-18 00:39:53.000", "yyyy-MM-dd HH:mm:ss"));
	}
    
    
}
