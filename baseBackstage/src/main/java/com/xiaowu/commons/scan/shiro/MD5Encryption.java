package com.xiaowu.commons.scan.shiro;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import com.xiaowu.commons.utils.CommonUtils;

/**
 * shiro md5加密
 * @author WH
 *
 */
public class MD5Encryption {

	/**
	 * 加密
	 * @param source 原始密码
	 * @param salt	盐
	 * @return
	 */
	public static String encryption(String source,String salt){
		
		//散列次数
		int hashIterations = 1;
		//上边散列1次：f3694f162729b7d0254c6e40260bf15c
		//上边散列2次：36f2dfa24d0a9fa97276abbe13e596fc
		
		
		/*//构造方法中：
		//第一个参数：明文，原始密码 
		//第二个参数：盐，通过使用随机数
		//第三个参数：散列的次数，比如散列两次，相当 于md5(md5(''))
		Md5Hash md5Hash = new Md5Hash(source, salt, hashIterations);
		
		String password_md5 =  md5Hash.toString();
		System.out.println(password_md5);*/
		//第一个参数：散列算法 
		SimpleHash simpleHash = new SimpleHash("md5", source, salt, hashIterations);
		return simpleHash.toString();
	}
	
	public static void main(String[] args) {
		String salt = CommonUtils.getUUID();
		System.out.println(salt);

		String salt1 = new SecureRandomNumberGenerator().nextBytes().toHex();//shiro自带盐生成器
		System.out.println(salt1);
		
		System.out.println(CommonUtils.getSalt());
		
		/*	System.out.println(encryption("1", salt));
	*/
		System.out.println(encryption("1", "9458c32739e329c9b6a2d52fadf503e3"));
	}
	
}
