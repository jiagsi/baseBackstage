package com.xiaowu.commons.scan;

import java.io.Serializable;


/**
 * 操作结果
 * @author WH
 *
 */
public class Result implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6281339837037581412L;

	private Integer code ;

    private String msg ;


	public Result(Integer code,String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
