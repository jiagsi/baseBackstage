package com.xiaowu.web.vo;

import java.util.List;

import com.xiaowu.web.model.Resource;

/**
 * 用户身份信息，存入session 由于tomcat将session会序列化在本地硬盘上，所以使用Serializable接口
 * 
 */
public class ActiveUser implements java.io.Serializable {
	private int userid;//用户id（主键）
	private String userloginname;// 用户账号
	private String username;// 用户名称

	private List<Resource> menus;// 菜单

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserloginname() {
		return userloginname;
	}

	public void setUserloginname(String userloginname) {
		this.userloginname = userloginname;
	}

	
	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public List<Resource> getMenus() {
		return menus;
	}

	public void setMenus(List<Resource> menus) {
		this.menus = menus;
	}

	
	
}
