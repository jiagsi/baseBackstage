package com.xiaowu.web.dao;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.xiaowu.web.model.Resource;

public interface ResourceMapper extends Mapper<Resource>{
   
	 /**
     * 根据用户Id查询未禁用的菜单
     * @param id 用户ID
     * @return
     */
    List<Resource> selectMenuByUserId(Resource resource);
}