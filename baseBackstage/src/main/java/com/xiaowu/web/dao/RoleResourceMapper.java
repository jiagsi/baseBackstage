package com.xiaowu.web.dao;

import tk.mybatis.mapper.common.Mapper;

import com.xiaowu.web.model.RoleResource;

public interface RoleResourceMapper extends Mapper<RoleResource>{
  
}