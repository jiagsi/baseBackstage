package com.xiaowu.web.dao;

import com.xiaowu.web.model.Syslog;
import tk.mybatis.mapper.common.Mapper;

public interface SyslogMapper extends Mapper<Syslog> {
}