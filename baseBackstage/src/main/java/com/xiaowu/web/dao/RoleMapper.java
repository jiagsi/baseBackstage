package com.xiaowu.web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.xiaowu.web.model.Role;

public interface RoleMapper extends Mapper<Role>{
   
}