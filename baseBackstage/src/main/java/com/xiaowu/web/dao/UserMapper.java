package com.xiaowu.web.dao;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

import com.xiaowu.web.model.User;
import com.xiaowu.web.vo.UserVo;

public interface UserMapper extends Mapper<User>{
   
	/**
	 * 根据条件查询
	 * @param map
	 * @return
	 */
    List<UserVo> selectAllByMap(Map<String,Object> map);
	
    
    /**
     * 修改用户密码
     * @return
     */
    int updateUserPassword(Map<String,Object> map);
}