package com.xiaowu.web.controller.report;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaowu.commons.utils.ExportCSV;
import com.xiaowu.commons.utils.ExportExcel;
import com.xiaowu.commons.utils.Servlets;
import com.xiaowu.web.model.Syslog;
import com.xiaowu.web.service.ISysLogService;

@RequestMapping("sysLog")
@Controller
public class SysLogController {

	@Autowired
	private ISysLogService sysLogService;
	
	/**
	 * 日志  页面
	 * @return
	 */
	@RequestMapping("sysLogPage")
	public String sysLogPage(Model model){
		return "report/sysLogList";
	}
	
	/**
	 * 日志  数据
	 * @return
	 */
	@RequestMapping("sysLogData")
	public String sysLogData(
			@RequestParam(required = false, defaultValue = "1") int page,
            @RequestParam(required = false, defaultValue = "10") int rows,
            Model model,
            ServletRequest reuqest) throws UnsupportedEncodingException{
		Map<String,Object> map = Servlets.getParameters(reuqest);
		PageHelper.startPage(page,rows);
		List<Syslog> list = sysLogService.selectAllByMap(map);
		PageInfo<Syslog> pageInfo = new PageInfo<Syslog>(list);
		model.addAttribute("pageInfo",pageInfo);
		
		return "report/sysLogList";
	}
	
	/**
	 * 日志 导出excel
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("sysLogExcelExport")
	public void sysLogExcelExport(HttpServletRequest reuqest,HttpServletResponse response) throws IOException{
		Map<String,Object> map = Servlets.getParameters(reuqest);
		List<Syslog> list = sysLogService.selectAllByMap(map);
	
		String title= "操作日志";
		String[][] headers = {
				{"登录名","loginName"},
				{"操作名称","roleName"},
				{"操作详细","optContent"},
				{"IP","clientIp"},
				{"操作时间","createTime"}
		};
		ExportExcel<Syslog> ex = new ExportExcel<Syslog>();
		//执行excel导出
		ex.exportExcelSXSSByListObject(title, headers, list,"yyyy-MM-dd HH:mm:ss",response);
		
	}
	
	/**
	 *  日志  导出csv
	 * @param reuqest
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping("sysLogCSVExport")
	public void sysLogCSVExport(HttpServletRequest reuqest,HttpServletResponse response) throws IOException{
		Map<String,Object> map = Servlets.getParameters(reuqest);
		List<Syslog> list = sysLogService.selectAllByMap(map);
		
		String title= "操作日志";
		String[][] headers = {
				{"登录名","loginName"},
				{"操作名称","roleName"},
				{"操作详细","optContent"},
				{"IP","clientIp"},
				{"操作时间","createTime"}
		};
		ExportCSV<Syslog> csv = new ExportCSV<Syslog>();
		csv.exportCSVByList(list, headers, response, title, "yyyy-MM-dd HH:mm:ss");
		
	}
	
}
