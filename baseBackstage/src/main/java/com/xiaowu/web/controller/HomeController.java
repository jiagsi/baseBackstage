package com.xiaowu.web.controller;

import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiaowu.commons.scan.shiro.MD5Encryption;
import com.xiaowu.commons.utils.CommonUtils;
import com.xiaowu.commons.utils.Servlets;
import com.xiaowu.web.model.User;
import com.xiaowu.web.service.IUserService;
import com.xiaowu.web.vo.ActiveUser;

/**
 * 首页
 * @author WH
 *
 */
@Controller
@RequestMapping("home")
public class HomeController {

	@Autowired
	private IUserService userService;
	
	/**
	 * 首页
	 * @param model
	 * @return
	 */
	@RequestMapping("index")
	public String index(Model model){
		Subject subject = SecurityUtils.getSubject();  
    	ActiveUser activeUser = (ActiveUser)subject.getPrincipals().getPrimaryPrincipal();
    	model.addAttribute("menus",activeUser.getMenus());
    	return "home";
	}
	

    /**
     * 退出
     * @return
     */
    @RequestMapping("/exit")
    public String logout(){
    	Subject subject = SecurityUtils.getSubject();  
    	subject.logout();
    	return "login";
    	
    }
    
    /**
     * 修改密码 页面
     * @return
     */
    @RequestMapping("updatePasswordPage")
    public String updatePasswordPage(){
    	return "home/updatePassword";
    }
    
    /**
     * 验证密码是否正确
     * @return
     */
    @ResponseBody
    @RequestMapping("checkAffirmPassword")
    public JSONObject checkAffirmPassword(String param){
    	JSONObject json = new JSONObject();
    	
    	//{ "info":"数据提交成功！", "status":"y" }
    	if(StringUtils.isBlank(param)){
    		json.put("info", "旧密码不能为空");
        	json.put("status", "n");
        	return json;
    	}

       	Subject subject = SecurityUtils.getSubject();  
       	ActiveUser activeUser = (ActiveUser)subject.getPrincipals().getPrimaryPrincipal();
       	//得到用户ID
       	int userid = activeUser.getUserid();
       	User queryUser = new User();
       	queryUser.setId(userid);
       	User user = userService.selectById(queryUser);
       	String password = user.getPassword();
       	String formerPassword = MD5Encryption.encryption(param, user.getSalt());
       	if(!password.equals(formerPassword)){
       		json.put("info", "旧密码错误，请重新输入");
        	json.put("status", "n");
        	return json;
       	}
    	json.put("info", "旧密码验证成功");
    	json.put("status", "y");
    	return json;
    }
    
    /**
     * 修改密码
     * @param reuqest
     * @return
     */
    @ResponseBody
    @RequestMapping("updatePassword")
    public int updatePassword(ServletRequest reuqest){
		Map<String,Object> map = Servlets.getParameters(reuqest);
		int updateCount = 0;
		//旧密码
		String password = map.get("password")+"";
		//密码为空
    	if(StringUtils.isBlank(password)){
        	return updateCount;
    	}
    	Subject subject = SecurityUtils.getSubject();  
       	ActiveUser activeUser = (ActiveUser)subject.getPrincipals().getPrimaryPrincipal();
       	//得到用户ID
       	int userid = activeUser.getUserid();
       	User queryUser = new User();
       	queryUser.setId(userid);
       	User user = userService.selectById(queryUser);
       	String oldPassword = user.getPassword();
       	String formerPassword = MD5Encryption.encryption(password, user.getSalt());
        //旧密码错误
       	if(!oldPassword.equals(formerPassword)){
       		return updateCount;
       	}
       	map.put("password", formerPassword);
       	//新密码  生成新的盐
       	String salt = CommonUtils.getSalt();
		map.put("salt", salt);
		map.put("updatePassword", MD5Encryption.encryption(map.get("updatePassword")+"", salt));
       	map.put("id", userid);
    	updateCount = userService.updateUserPassword(map);
    	return updateCount;
    }
    
}
