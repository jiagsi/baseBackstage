package com.xiaowu.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaowu.commons.utils.Servlets;
import com.xiaowu.commons.utils.StaticParameter;
import com.xiaowu.web.model.Resource;
import com.xiaowu.web.model.Role;
import com.xiaowu.web.model.RoleResource;
import com.xiaowu.web.service.IResourceService;
import com.xiaowu.web.service.IRoleResourceService;
import com.xiaowu.web.service.IRoleService;

/**
 * 角色管理
 * @author WH
 *
 */
@Controller
@RequestMapping("role")
public class RoleController {

	@Autowired
	private IRoleService roleService;
	
	@Autowired
	private IResourceService resourceService;
	
	@Autowired
	private IRoleResourceService roleResourceService;
	
	/**
	 * 角色  页面
	 * @return
	 */
	@RequestMapping("rolePage")
	public String rolePage(Model model){
		return "system/roleList";
	}
	
	/**
	 * 角色  数据
	 * @return
	 */
	@RequestMapping("roleData")
	public String roleData( 
			@RequestParam(required = false, defaultValue = "1") int page,
            @RequestParam(required = false, defaultValue = "10") int rows,
            Model model,
            ServletRequest reuqest) throws UnsupportedEncodingException{
		Map<String,Object> map = Servlets.getParameters(reuqest);
		PageHelper.startPage(page,rows);
		List<Role> list = roleService.selectAllByMap(map);
		PageInfo<Role> pageInfo = new PageInfo<Role>(list);
		model.addAttribute("pageInfo",pageInfo);
		return "system/roleList";
	}
	
	/**
	 * 角色  授权 页面
	 * @return
	 */
	@RequestMapping("roleAuthorityPage")
	public String roleAuthorityPage(Model model,Integer id){
		model.addAttribute("id", id);
		return "system/roleAuthority";
	}
	
	/**
	 * 得到角色的授权资源信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resourceData")
	public List<Map<String,Object>> resourceData(ServletRequest reuqest){
		Map<String,Object> map = Servlets.getParameters(reuqest);
		//得到所有资源
		List<Resource> list = resourceService.selectAllByMap(map);
		//得到自己的资源
		List<RoleResource> roleResourceList = roleResourceService.selectAllByRoleId(Integer.parseInt(map.get("id")+""));
		Map<Integer,Boolean> resourceMap = new HashMap<Integer,Boolean>();
		for (RoleResource roleResource : roleResourceList) {
			resourceMap.put(roleResource.getResourceId(), true);
		}
		
		List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
		Map<String,Object> map1 = null;
		for (Resource resource : list) {
			map1 = new HashMap<String,Object>();
			map1.put("id", resource.getId());
			map1.put("name", resource.getName());
			map1.put("pid", resource.getPid());
			map1.put("isResource", resourceMap.get(resource.getId()));
			listMap.add(map1);
		}
		return listMap;
	}
	
	/**
	 * 角色 授权 操作
	 * @return
	 */
	@ResponseBody
	@RequestMapping("roleAuthorityOperation")
	public String roleAuthorityOperation(Integer id,String resources){
		try {
			roleService.updateRoleResource(id, resources);
			return "success";
		} catch (Exception e) {
			return "error";
		}
	}
	
	
	/**
	 * 角色 编辑页面
	 * @return
	 */
	@RequestMapping("roleEditPage")
	public String roleEditPage(Model model,Role role){
		if(null!=role && null!=role.getId()){
			Role roleInfo = roleService.selectById(role);
			model.addAttribute("role", roleInfo);
		}
		return "system/roleEdit";
	}
	

	/**
	 * 角色 添加
	 * @return
	 */
	@ResponseBody
	@RequestMapping("roleAdd")
	public int roleAdd(Role role){
		return roleService.addRole(role);
	}
	
	/**
	 * 角色 修改
	 * @return
	 */
	@ResponseBody
	@RequestMapping("roleUpdate")
	public int roleUpdate(Role role){
		return roleService.updateRole(role);
	}
	
	/**
	 * 角色 删除
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping("roleDelete")
	public int roleDelete(Role role){
		return roleService.deleteRole(role);
	}
	
	
	
}
