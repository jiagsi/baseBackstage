package com.xiaowu.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaowu.commons.scan.shiro.MD5Encryption;
import com.xiaowu.commons.utils.CommonUtils;
import com.xiaowu.commons.utils.Servlets;
import com.xiaowu.commons.utils.StaticParameter;
import com.xiaowu.web.model.Role;
import com.xiaowu.web.model.User;
import com.xiaowu.web.service.IRoleService;
import com.xiaowu.web.service.IUserService;
import com.xiaowu.web.vo.UserVo;

/**
 * 用户管理
 * @author WH
 *
 */
@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IRoleService roleService;
	
	/**
	 * 用户  页面
	 * @return
	 */
	@RequestMapping("userPage")
	public String userPage(Model model){
		return "system/userList";
	}
	
	
	/**
	 * 用户  数据
	 * @return
	 */
	@RequestMapping("userData")
	public String userData( 
			@RequestParam(required = false, defaultValue = "1") int page,
            @RequestParam(required = false, defaultValue = "10") int rows,
            Model model,
            ServletRequest reuqest) throws UnsupportedEncodingException{
		Map<String,Object> map = Servlets.getParameters(reuqest);
		PageHelper.startPage(page,rows);
		List<UserVo> list = userService.selectAllByMap(map);
		PageInfo<UserVo> pageInfo = new PageInfo<UserVo>(list);
		model.addAttribute("pageInfo",pageInfo);
		return "system/userList";
	}
	
	/**
	 * 用户 编辑页面
	 * @return
	 */
	@RequestMapping("userEditPage")
	public String userEditPage(Model model,User user){
		Map<String,Object> map = new HashMap<String,Object>();
		//查询所有启用中的角色
		map.put("status", StaticParameter.statusEnable);
		List<Role> roles = roleService.selectAllByMap(new HashMap<String, Object>());
		model.addAttribute("roles", roles);
		if(null!=user && null!=user.getId()){
			User userInfo =userService.selectById(user);
			model.addAttribute("user", userInfo);
		}
		
		return "system/userEdit";
	}
	
	
	/**
	 * 用户 添加
	 * @return
	 */
	@ResponseBody
	@RequestMapping("userAdd")
	public int userAdd(User user,HttpServletRequest request){
		user.setCreatedate(new Date());
		user.setSalt(CommonUtils.getUUID());
		//加密
		user.setPassword(MD5Encryption.encryption(user.getPassword(), user.getSalt()));
		int addNumber = userService.addUser(user);
		return addNumber;
	}
	
	/**
	 * 用户 修改
	 * @return
	 */
	@ResponseBody
	@RequestMapping("userUpdate")
	public int userUpdate(User user){
		if(StringUtils.isNotBlank(user.getPassword())){
			user.setSalt(CommonUtils.getUUID());
			user.setPassword(MD5Encryption.encryption(user.getPassword(), user.getSalt()));
		}else{
			//没输入密码，则不修改
			user.setPassword(null);
		}
		return userService.updateUser(user);
	}
	
	/**
	 * 用户 删除
	 * @return
	 */
	@ResponseBody
	@RequestMapping("userDelete")
	public int userDelete(User user){
		int deleteNumber= userService.deleteUser(user);
		return deleteNumber;
	}
	
}
