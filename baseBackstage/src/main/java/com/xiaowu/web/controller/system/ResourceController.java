package com.xiaowu.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaowu.commons.utils.Servlets;
import com.xiaowu.web.model.Resource;
import com.xiaowu.web.service.IResourceService;

/**
 * 资源管理
 * @author WH
 *
 */
@Controller
@RequestMapping("resource")
public class ResourceController {

	@Autowired
	private IResourceService resourceService;
	
	/**
	 * 资源 页面
	 * @return
	 */
	@RequestMapping("resourcePage")
	public String resourcePage(){
		return "system/resourceList";
	}
	
	/**
	 * 资源 数据
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("resourceData")
	public String resourceData(
            @RequestParam(required = false, defaultValue = "1") int page,
            @RequestParam(required = false, defaultValue = "10") int rows,
            Model model,
            ServletRequest reuqest) throws UnsupportedEncodingException{
		
		Map<String,Object> map = Servlets.getParameters(reuqest);
		
		PageHelper.startPage(page,rows);
		List<Resource> list = resourceService.selectAllByMap(map);
		PageInfo<Resource> pageInfo = new PageInfo<Resource>(list);
		model.addAttribute("pageInfo",pageInfo);
		return "system/resourceList";
	}
	
	/**
	 * 资源菜单 数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resourceMenuData")
	public List<Resource> resourceMenuData(ServletRequest reuqest){
		Map<String,Object> map = Servlets.getParameters(reuqest);
		List<Resource> list = resourceService.selectAllByMap(map);
		return list;
	}
	
	/**
	 * 资源 - 编辑页面
	 * @return
	 */
	@RequestMapping("resourceEditPage")
	public String resourceEditPage(Model model,Resource resource){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("resourcetype", 0);
		List<Resource> list = resourceService.selectAllByMap(map);
		model.addAttribute("list", list);
		
		if(null != resource && null != resource.getId()){
			Resource resourceInfo = resourceService.selectById(resource);
			model.addAttribute("resource", resourceInfo);
		}
		
		return "system/resourceEdit";
	}

	/**
	 * 资源 - 添加
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resourceAdd")
	public int resourceAdd(Resource resource){
		resource.setCreatedate(new Date());
		int addNumber = resourceService.addResource(resource);
		return addNumber;
	}
	
	/**
	 * 资源 - 修改
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resourceUpdate")
	public int resourceUpdate(Resource resource){
		int addNumber = resourceService.updateResource(resource);
		return addNumber;
	}
	
	/**
	 * 资源 - 删除
	 * @return
	 */
	@ResponseBody
	@RequestMapping("resourceDelete")
	public int resourceDelete(Resource resource){
		int addNumber = resourceService.deleteResource(resource);
		return addNumber;
	}
	
	
}
