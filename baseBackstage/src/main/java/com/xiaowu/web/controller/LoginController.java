package com.xiaowu.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import com.sun.xml.internal.fastinfoset.sax.Properties;

/**
 * 登录
 * @author WH
 *
 */
@Controller
@RequestMapping
public class LoginController {
	
	/**
     * 登陆
     * @return
     */
    @RequestMapping("login")
    public ModelAndView login(HttpServletRequest reuqest){
    	
    	//是否是AJAX请求
        String requestType = reuqest.getHeader("X-Requested-With");
    	
    	 String error = null;  
    	//账号
    	String loginname = reuqest.getParameter("loginname");  
        //密码
    	String password = reuqest.getParameter("password");  

        ModelAndView model = new ModelAndView();
        FastJsonJsonView view = new FastJsonJsonView();  
    	if(StringUtils.isBlank(loginname) || StringUtils.isBlank(password)){
    			error="请输入账号密码";
    		  // model.addAttribute("error", error);
              //return "login";
    			
			if(requestType!=null){
				Map<String, Object> attributes = new HashMap<String, Object>();  
   		         attributes.put("state", "userNotLogin");  
   		         view.setAttributesMap(attributes);  
   		         model.setView(view);   
   		         return model; 
			}
            model.setViewName("login");
      		model.addObject("error", error);
      		return model;
    	}
        
    	Subject subject = SecurityUtils.getSubject();  
        UsernamePasswordToken token = new UsernamePasswordToken(loginname,password);
        
        try {  
            subject.login(token);  
        } catch (UnknownAccountException e) {  
            error = "用户名不存在";  
        } catch (IncorrectCredentialsException e) {  
            error = "密码错误";  
        } catch (AuthenticationException e) {  
            //其他错误，比如锁定，如果想单独处理单独catch处理  
            error = "其他错误：" + e.getMessage();  
        }  
        if(error!=null){
        	if(requestType!=null){
				Map<String, Object> attributes = new HashMap<String, Object>();  
   		         attributes.put("state", "userNotLogin");  
   		         view.setAttributesMap(attributes);  
   		         model.setView(view);   
   		         return model; 
			}
        	//model.addAttribute("error", error);
        	//return "login";
        	model.setViewName("login");
       		model.addObject("error", error);
       		return model;
        }else{
        	//return "redirect:/home/index"; 
        	model.setViewName("redirect:/home/index");
       		return model;
        }
      	
    }
  
    
}
