package com.xiaowu.web.service;

import java.util.List;
import java.util.Map;

import com.xiaowu.web.model.User;
import com.xiaowu.web.vo.UserVo;

/**
 * 用户 Service接口
 * @author WH
 *
 */
public interface IUserService {

	/**
	 * 根据查询条件查询用户一条数据
	 * @param loginname
	 * @return
	 */
	public User findUserByLoginname(User user);
	
	
	/**
	 * 根据条件查询
	 * @param map
	 * @return
	 */
    List<UserVo> selectAllByMap(Map<String,Object> map);
    
    
    /**
     * 用户 添加
     * @param user
     * @return
     */
    int addUser(User user);
    
    /**
     * 用户 修改
     * @param user
     * @return
     */
    int updateUser(User user);
	
    /**
     * 用户 删除
     * @param id
     * @return
     */
    int deleteUser(User user);
    
   
    /**
     * 根据ID查询user
     * @param user
     * @return
     */
    User selectById(User user);
    
    /**
     * 修改用户密码
     * @return
     */
    int updateUserPassword(Map<String,Object> map);
   
}
