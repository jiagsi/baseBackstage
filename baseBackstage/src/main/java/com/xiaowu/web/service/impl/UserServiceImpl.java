package com.xiaowu.web.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;

import com.xiaowu.web.dao.UserMapper;
import com.xiaowu.web.model.Resource;
import com.xiaowu.web.model.User;
import com.xiaowu.web.service.IUserService;
import com.xiaowu.web.vo.UserVo;


@Service("userService")
public class UserServiceImpl extends BaseService<User> implements IUserService {

	@Autowired
	private UserMapper userMapper;
	
	
	@Override
	public User findUserByLoginname(User user) {
		return userMapper.selectOne(user);
	}

	
	@Override
	public List<UserVo> selectAllByMap(Map<String, Object> map) {
		return userMapper.selectAllByMap(map);
	}
	
	@Override
	public int addUser(User user) {
		return userMapper.insertSelective(user);
	}
	
	@Override
	public int updateUser(User user) {
		Example example = new Example(Resource.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id",user.getId());
		return userMapper.updateByExampleSelective(user, example);
	}
	
	@Override
	public int deleteUser(User user) {
		return userMapper.delete(user);
	}
	
	@Override
	public User selectById(User user) {
		return userMapper.selectOne(user);
	}
	
	@Override
	public int updateUserPassword(Map<String,Object> map) {
		return userMapper.updateUserPassword(map);
	}
}
