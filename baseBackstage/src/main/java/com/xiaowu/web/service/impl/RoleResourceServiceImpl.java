package com.xiaowu.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;

import com.xiaowu.web.dao.RoleResourceMapper;
import com.xiaowu.web.model.RoleResource;
import com.xiaowu.web.service.IRoleResourceService;


@Service("roleResourceService")
public class RoleResourceServiceImpl extends BaseService<RoleResource> implements IRoleResourceService {

	@Autowired
	private RoleResourceMapper roleResourceMapper;
	
	@Override
	public List<RoleResource> selectAllByRoleId(Integer roleId) {
		Example example = new Example(RoleResource.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("roleId",roleId);
		return roleResourceMapper.selectByExample(example);
	}

}
