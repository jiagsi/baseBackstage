package com.xiaowu.web.service;

import java.util.List;

import com.xiaowu.web.model.RoleResource;

public interface IRoleResourceService {

	/**
	 * 根据角色ID查询用户资源信息
	 * @param roleId
	 * @return
	 */
    List<RoleResource> selectAllByRoleId(Integer roleId);
	
}
