package com.xiaowu.web.service;

import java.util.List;
import java.util.Map;

import com.xiaowu.web.model.Resource;

/**
 * 资源Service接口
 * @author WH
 *
 */
public interface IResourceService {


	/**
	 *  根据条件查询
	 * @param map 查询条件
	 * @return
	 */
    List<Resource> selectAllByMap(Map<String,Object> map);
    
    /**
     * 根据用户Id查询未禁用的资源
     * @param id 用户ID
     * @return
     */
    List<Resource> selectMenuByUserId(Resource resource);
    
    /**
     * 添加资源数据
     * @param resource
     * @return
     */
    int addResource(Resource resource);
    
    /**
     * 根据ID查询资源数据
     * @param id
     * @return
     */
    Resource selectById(Resource resource);
    
    /**
     * 添加资源数据
     * @param resource
     * @return
     */
    int updateResource(Resource resource);
    
    /**
     * 删除资源数据
     * @param resource
     * @return
     */
    int deleteResource(Resource resource);
    
}
