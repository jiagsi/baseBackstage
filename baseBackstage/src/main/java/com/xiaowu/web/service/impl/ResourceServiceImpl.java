package com.xiaowu.web.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;

import com.xiaowu.web.dao.ResourceMapper;
import com.xiaowu.web.model.Resource;
import com.xiaowu.web.service.IResourceService;

@Service("resourceService")
public class ResourceServiceImpl extends BaseService<Resource> implements IResourceService{

	@Autowired
	private ResourceMapper resourceMapper;
	
	public List<Resource> selectAllByMap(Map<String,Object> map) {
		Example example = new Example(Resource.class);
		Example.Criteria criteria = example.createCriteria();
	
		//资源类型（0菜单，1功能）
		if(StringUtils.isNotBlank(MapUtils.getString(map, "resourcetype"))){
			criteria.andEqualTo("resourcetype",MapUtils.getString(map, "resourcetype"));
		}
		
		criteria.andEqualTo("pid",map.get("pid"));
		
		//排序字段
		example.setOrderByClause("seq");
		
		return resourceMapper.selectByExample(example);
	}


	@Override
	public List<Resource> selectMenuByUserId(Resource resource) {
		return resourceMapper.selectMenuByUserId(resource);
	}
	
	
	@Override
	public int addResource(Resource resource) {
		return resourceMapper.insertSelective(resource);
	}
	
	@Override
	public Resource selectById(Resource resource) {
		return resourceMapper.selectOne(resource);
	}
	
	@Override
	public int updateResource(Resource resource) {
		Example example = new Example(Resource.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id",resource.getId());
		return resourceMapper.updateByExampleSelective(resource, example);
		//return resourceMapper.updateByPrimaryKeySelective(resource);
	}
	
	@Override
	public int deleteResource(Resource resource) {
		return resourceMapper.delete(resource);
	}
	
}
