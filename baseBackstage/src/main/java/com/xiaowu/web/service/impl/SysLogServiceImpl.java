package com.xiaowu.web.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import com.xiaowu.web.dao.SyslogMapper;
import com.xiaowu.web.model.Role;
import com.xiaowu.web.model.Syslog;
import com.xiaowu.web.service.ISysLogService;

@Service("sysLogService")
public class SysLogServiceImpl extends BaseService<Syslog> implements ISysLogService {

	@Autowired
	private SyslogMapper syslogMapper;
	
	@Override
	public int addSysLog(Syslog syslog) {
		return save(syslog);
	}
	
	@Override
	public List<Syslog> selectAllByMap(Map<String, Object> map) {
		Example example = new Example(Syslog.class);
		Example.Criteria criteria = example.createCriteria();
		//登录名
		if (StringUtil.isNotEmpty(MapUtils.getString(map, "loginName"))) {
            criteria.andLike("loginName", "%" + MapUtils.getString(map, "loginName") + "%");
        }
		//操作名称
		if (StringUtil.isNotEmpty(MapUtils.getString(map, "roleName"))) {
            criteria.andLike("roleName", "%" + MapUtils.getString(map, "roleName") + "%");
        }
		//IP
		if (StringUtil.isNotEmpty(MapUtils.getString(map, "clientIp"))) {
            criteria.andLike("clientIp", "%" + MapUtils.getString(map, "clientIp") + "%");
        }
		/*//状态（0启用，1禁用）
		if(StringUtils.isNotBlank(MapUtils.getString(map, "status"))){
			criteria.andEqualTo("status",MapUtils.getString(map, "status"));
		}
		//名称
		if (StringUtil.isNotEmpty(MapUtils.getString(map, "name"))) {
            criteria.andLike("name", "%" + MapUtils.getString(map, "name") + "%");
        }
		//描述
		if (StringUtil.isNotEmpty(MapUtils.getString(map, "description"))) {
            criteria.andLike("description", "%" + MapUtils.getString(map, "description") + "%");
        }*/
		return syslogMapper.selectByExample(example);
	}

}
