package com.xiaowu.web.service;

import java.util.List;
import java.util.Map;

import com.xiaowu.web.model.Role;

/**
 * 角色 Service接口
 * @author WH
 *
 */
public interface IRoleService {

	/**
	 * 根据条件查询
	 * @param holdChange
	 * @return
	 */
    List<Role> selectAllByMap(Map<String,Object> map);
    
    
    /**
     * 角色资源 修改
     * @param id
     * @param resource
     * @return
     */
    int updateRoleResource(Integer id,String resource);
	
    /**
     * 角色 添加
     * @param role
     * @return
     */
    int addRole(Role role);
    
    /**
     * 角色 修改
     * @param role
     * @return
     */
    int updateRole(Role role);
    
    /**
     * 角色 删除
     * @param role
     * @return
     */
    int deleteRole(Role role);
    
    /**
     * 根据ID查询角色
     * @param role
     * @return
     */
    Role selectById(Role role);
}
