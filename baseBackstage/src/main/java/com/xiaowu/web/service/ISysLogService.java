package com.xiaowu.web.service;

import java.util.List;
import java.util.Map;

import com.xiaowu.web.model.Role;
import com.xiaowu.web.model.Syslog;

public interface ISysLogService {

	int addSysLog(Syslog syslog);
	
	

	/**
	 * 根据条件查询
	 * @param holdChange
	 * @return
	 */
    List<Syslog> selectAllByMap(Map<String,Object> map);
}
